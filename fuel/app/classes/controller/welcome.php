<?php

/**
 * Fuel is a fast, lightweight, community driven PHP5 framework
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://fuelphp.com
 */

use Fuel\Core\Fuel;

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller
{
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/index'));
		// return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('welcome/index'));
	}

	/** 
	 * member benefits 
	 * */
	public function action_member_benefits($benefitsType = '')
	{
		if ($benefitsType == '') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits');
		} elseif ($benefitsType == 'classic') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits-classic');
		} elseif ($benefitsType == 'black') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits-black');
		} elseif ($benefitsType == 'vip') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits-vip');
		} else {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits');
		}

		return \Fuel\Core\Response::forge($view);
	}

	/** 
	 * member benefits 
	 * */
	public function action_member_benefits_saudi($benefitsType = '')
	{
		if ($benefitsType == '') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits');
		} elseif ($benefitsType == 'classic') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits-saudi-classic');
		} elseif ($benefitsType == 'black') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits-saudi-black');
		} elseif ($benefitsType == 'vip') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits-saudi-vip');
		} else {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits');
		}

		return \Fuel\Core\Response::forge($view);
	}

		/** 
	 * member benefits 
	 * */
	public function action_member_benefits_egypt($benefitsType = '')
	{
		if ($benefitsType == '') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits');
		} elseif ($benefitsType == 'classic') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits-egypt-classic');
		} elseif ($benefitsType == 'black') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits-egypt-black');
		} elseif ($benefitsType == 'vip') {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits-egypt-vip');
		} else {
			$view = \Fuel\Core\View::forge('worldgate/member-benefits');
		}

		return \Fuel\Core\Response::forge($view);
	}
	// hotel hist
	public function action_hotel_list()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/hotel_list'));
	}

	public function action_flight_list()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/flight_list'));
	}

	public function action_car_rent()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/car_rent'));
	}

	public function action_about_us()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/about_us'));
	}

	public function action_blogs()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/blogs'));
	}

	public function action_contact_us()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/contact_us'));
	}

	public function action_term_n_conditions()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/term_n_condition'));
	}

	public function action_privacy_policy()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/privacy_policy'));
	}

	public function action_login()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/login'));
	}

	public function action_register()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/register'));
	}


	public function action_paytab()
	{
		$data = array(); //stores variables going to views

		if (\Fuel\Core\Input::get('cart_amount') && \Fuel\Core\Input::get('cart_currency')) {
			return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/paytab', [
				"cartAmount" => \Fuel\Core\Input::get('cart_amount'),
				"cartCurrency" => \Fuel\Core\Input::get('cart_currency'),
			]));
		} else {
			return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/paytab', [
				"cartAmount" => '',
				"cartCurrency" => '',
			]));
		}
	}

	public function action_paywtab()
	{

		$curl = curl_init();


		$payloadString = "{\"profile_id\":\"83901\",\"tran_type\":\"sale\",\"tran_class\":\"ecom\",\"cart_description\":\"Memebership payment\",\"cart_id\":\"2323\",\"cart_currency\":\"EGP\",\"cart_amount\":1,\"callback\":\"" . \Fuel\Core\Uri::base(false) . 'welcome/payment' . "\",\"return\":\"" . \Fuel\Core\Uri::base(false) . 'welcome' . "\"}";
		$payloadObj = json_decode($payloadString);
		$payloadObj->cart_amount = \Fuel\Core\Input::post('cart_amount');
		$payloadObj->cart_id = \Fuel\Core\Input::post('cart_id');

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://secure-egypt.paytabs.com/payment/request",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($payloadObj),
			CURLOPT_HTTPHEADER => array(
				"authorization: SNJNZGHGGN-J2MBWMGJ9J-HHRMGHGJMG",
				"content-type: application/json"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$decode = json_decode($response);
			echo '<center><h1>Please wait you will be redirect to the payment page </h1></center>';
			// echo $decode->transaction->url;
			\Fuel\Core\Response::redirect($decode->redirect_url, 'refresh');
		}
	}

	public function action_tap()
	{
		$data = array(); //stores variables going to views

		if (\Fuel\Core\Input::get('cart_amount') && \Fuel\Core\Input::get('cart_currency')) {
			return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/tap', [
				"cartAmount" => \Fuel\Core\Input::get('cart_amount'),
				"cartCurrency" => \Fuel\Core\Input::get('cart_currency'),
			]));
		} else {
			return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/tap', [
				"cartAmount" => '',
				"cartCurrency" => '',
			]));
		}
	}



	public function action_pay()
	{

		$curl = curl_init();

		$payloadString = "{\"amount\":1,\"currency\":\"BHD\",\"threeDSecure\":true,\"save_card\":false,\"description\":\"Test Description\",\"statement_descriptor\":\"Sample\",\"metadata\":{\"udf1\":\"test 1\",\"udf2\":\"test 2\"},\"reference\":{\"transaction\":\"txn_0001\",\"order\":\"ord_0001\"},\"receipt\":{\"email\":false,\"sms\":true},\"customer\":{\"first_name\":\"test\",\"middle_name\":\"test\",\"last_name\":\"test\",\"email\":\"test@test.com\",\"phone\":{\"country_code\":\"965\",\"number\":\"50000000\"}},\"merchant\":{\"id\":\"6421736\"},\"source\":{\"id\":\"src_all\"},\"post\":{\"url\":\"" . \Fuel\Core\Uri::base(false) . 'welcome/payment' . "\"},\"redirect\":{\"url\":\"" . \Fuel\Core\Uri::base(false) . 'welcome/payment' . "\"}}";
		$payloadObj = json_decode($payloadString);
		$payloadObj->amount = \Fuel\Core\Input::post('cart_amount');
		$payloadObj->description = \Fuel\Core\Input::post('cart_description');
		$payloadObj->statement_descriptor = \Fuel\Core\Input::post('cart_description');
		$payloadObj->reference->transaction = \Fuel\Core\Input::post('cart_id');
		$payloadObj->customer->first_name = \Fuel\Core\Input::post('full_name');
		$payloadObj->customer->email = \Fuel\Core\Input::post('email_address');

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.tap.company/v2/charges",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($payloadObj),
			CURLOPT_HTTPHEADER => array(
				"authorization: Bearer sk_live_G3FiZXldje8cQfn5grNpV9Ry",
				"content-type: application/json"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$decode = json_decode($response);
			echo '<center><h1>Please wait you will be redirect to the payment page </h1></center>';
			// echo $decode->transaction->url;
			\Fuel\Core\Response::redirect($decode->transaction->url, 'refresh');
		}
	}

	public function action_payment()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('worldgate/payment', [
			"cartAmount" => '',
			"cartCurrency" => '',
		]));
	}

	/**
	 * The 404 action for the application.
	 *
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_v2()
	{
		return \Fuel\Core\Response::forge(\Fuel\Core\View::forge('welcome/v2'));
	}
}
