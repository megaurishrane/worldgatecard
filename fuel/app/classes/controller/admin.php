<?php
/**
* Fuel is a fast, lightweight, community driven PHP5 framework.
*
* @package    Fuel
* @version    1.7
* @author     Fuel Development Team
* @license    MIT License
* @copyright  2010 - 2015 Fuel Development Team
* @link       http://fuelphp.com
*/

/**
* The Welcome Controller.
*
* A basic controller example.  Has examples of how to set the
* response body and status.
*
* @package  app
* @extends  Controller
*/
class Controller_Admin extends Controller
{
    /**
    * The basic welcome message
    *
    * @access  public
    * @return  Response
    */
    public function action_index()
    {
        $view = View::forge('admin/base_layout');
        return $view;
    }
    
    /**
    * Serves the main.js file dynamically
    * Admin consists of different user level groups
    * WHY: because even for activators(example) all the files are loaded.
    * This decreases the files that are loaded and increases speed.
    */
    public function get_main() {
        $response = new \Fuel\Core\Response();
        
        // $auth = new \Nb\OAuth\Auth();
        // $group_id = $auth->getProperty('group');
        // $permissions = \Nb\Permissions::getForGroup($group_id);
        
        $body = \View::forge('admin2/main.js');
        
        $response->set_header('Content-type', 'text/javascript', true)
        ->body($body)
        ->send(true);
    }
    
    /**
    * The 404 action for the application.
    *
    * @access  public
    * @return  Response
    */
    public function action_404()
    {
        $view = View::forge('template/samplepageTemplate');
        $view->contain = View::forge('pages/utile/404');
        return $view;
    }
}