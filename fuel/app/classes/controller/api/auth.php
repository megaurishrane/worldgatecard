<?php

use Fuel\Core\Fuel;
use Fuel\Core\Input;

class Controller_Api_Auth extends \Fuel\Core\Controller_Rest
{

    public function get_index()
    {
        $this->response([
            'status' => true,
            'message' => 'auth api'
        ]);
    }

    public function get_valid()
    {
        try {
            $user = [];
            if ($this->user_id) {
                $user = \Fame\Auth\Users::instance()->get_one([
                    'id' => $this->user_id,
                ]);
                $user = \Fame\Auth\Users::instance()->parse($user);
            } else {
                throw new \Fame\Exception\UserException('User is not logged in');
            }

            $r = [
                'status' => true,
                'data' => [
                    'id' => $this->user_id,
                    'user' => $user,
                ],
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }


    // private function sendOtpEmail($email, $otp)
    // {
    //     $emailObj = \Email\Email::forge();
    //     $emailObj->from('no-reply-Fame@craftpip.com', 'FameShree verification')
    //         ->to($email)
    //         ->subject('Please verify your FameShree account')
    //         ->body("Thank you for registering with FameShree. \n Your otp is $otp")->send();
    // }


    // public function get_authenticate()
    // {
    //     try {
    //         $auth = \Fame\Auth\Auth::instance();
    //         $username = Input::get('username', false);
    //         $password = Input::get('password', false);

    //         list($user, $token) = $auth->login($username, $password);

    //         $user = \Fame\Auth\Users::instance()->parse($user);

    //         $r = [
    //             'status' => true,
    //             'data' => [
    //                 'user' => $user,
    //                 'token' => $token,
    //             ],
    //         ];
    //     } catch (\Exception $e) {
    //         $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
    //         $r = [
    //             'status' => false,
    //             'reason' => $e->getMessage(),
    //         ];
    //     }
    //     $this->response($r);
    // }

    public function get_create()
    {
        try {

            $p = Input::get();
            $userAuthObj = Fame\Auth\Users::instance();
            \Fuel\Core\DB::start_transaction();

            $createObj = $userAuthObj->create_user(
                $p['name'] . '-' . mt_rand(10, 10000),
                $p['email'],
                $p['mobile'],
                $p['password'],
                '01',
                [],
                '+973',
                ['name' => $p['name']]
            );

            \Fuel\Core\DB::commit_transaction();
            $r = [
                'status' => true,
                'data' => $createObj,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            \Fuel\Core\DB::rollback_transaction();

            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    // public function get_verify_otp()
    // {
    //     try {
    //         $otp = \Fuel\Core\DB::select('user_id')->from('users_otp_log')->where('otp', Input::get('otp'))->execute()->as_array();
    //         if (isset($otp[0]['user_id'])) {

    //             $update = \Fuel\Core\DB::update('users_otp_log')->set(['status' => 'active'])->where('otp', '=', Input::get('otp'))->execute();
    //             $update = \Fuel\Core\DB::update('users')->set([
    //                 'mobile_verified' => 1,
    //                 'account_active' => 1
    //             ])->where('id', '=', $otp[0]['user_id'])->execute();
    //             $r = [
    //                 'status' => true,
    //                 'data' => [],
    //                 'message' => 'OTP Verification successful. Your account is active.'
    //             ];
    //         } else {
    //             $r = [
    //                 'status' => false,
    //                 'data' => [],
    //                 'message' => 'Invalid OTP.'
    //             ];
    //         }
    //     } catch (Exception $e) {
    //         $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
    //         $r = [
    //             'status' => false,
    //             'reason' => $e->getMessage(),
    //         ];
    //     }

    //     $this->response($r);
    // }

    public
    function get_logout()
    {
        try {
            \Fame\Auth\Auth::instance()->logout();
            $r = [
                'status' => true,
            ];
        } catch (\Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }
        $this->response($r);
    }
}
