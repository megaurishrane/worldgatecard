<?php

/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 4/9/2018
 * Time: 4:51 PM
 */
class Controller_Api_Admin_Category extends \Fuel\Core\Controller_Rest
{
    /**
     * The basic Api_Admin_Category message
     *
     * @access  public
     * @return  Response
     */
    public function get_index()
    {
        $r = [
            'status' => true,
            'message' => 'Api_Admin_Category'
        ];
        $this->response($r);
    }

    public function get_list()
    {
        try {

            $data = Db::select()->from('category_mast')->where('parent_id', 0)->execute()->as_array();
            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    public function get_sub_category_list()
    {
        try {

            $data = Db::select()->from('category_mast')->where('parent_id', '<>', 0)->execute()->as_array();
            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }
}
