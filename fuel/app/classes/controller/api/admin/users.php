<?php

/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 4/9/2018
 * Time: 12:42 PM
 */
class Controller_Api_Admin_Users extends \Fuel\Core\Controller_Rest
{
    /**
     * The basic Api_Admin_Users message
     *
     * @access  public
     * @return  Response
     */
    public function get_index()
    {
        $r = [
            'status' => true,
            'message' => 'Api_Admin_Users',
            'more_messag' => 'someasdfasdf',
            'request_val' => Input::get('username')
        ];
        $this->response($r);
    }

    public function get_list()
    {
        try {
            $data = DB::select()->from('users')->where('group', 1)->execute()->as_array();

            foreach ($data as $k => $v) {
                $unSerialData = unserialize($v['profile_fields']);
                foreach ($unSerialData as $k2 => $v2) {
                    $data[$k][$k2] = $v2;
                }
            }

            $r = [
                'status' => true,
                'data' => $data
            ];

        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

}