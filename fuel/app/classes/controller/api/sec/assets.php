<?php

/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 7/10/2017
 * Time: 11:38 AM
 */
class Controller_Api_Sec_Assets extends Controller_Api_Sec_Check
{

    public function get_index()
    {
        return $this->response(array(
            'status' => true,
            'user_id' => $this->user_id,
            'version' => '1.0.0',
            'message' => 'Api_Sec_Assets controller . Test base api',
        ));
    }

    /**
     * @deprecated
     */
    public function get_all()
    {
        try {
            $data = Fame\AssetsMst::get_join(['user_id' => $this->user_id]);
            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    /**
     * Get sum of total asset  worth by group by
     */
    public function get_worth()
    {
        try {

            $i = Input::get('worth_by', 'main');
            $id = Input::get('id', 0);

            $data = Fame\AssetsMst::get_worth($this->user_id, $i, $id);

            $r = [
                'status' => true,
                'data' => $data
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    /**
     * Create a asset
     */
    public function get_create()
    {
        try {
            $data = [];
            $address = json_decode(Input::get('asset_address'), true);
            $amount = json_decode(Input::get('asset_amount'), true);
            $category = json_decode(Input::get('category'), true);

            //get mapped field from the database
            $map_address_field = DB::select()->from('field_mst')->where('name', 'in', array_keys($address))->execute()->as_array();
            $map_amount_field = DB::select()->from('field_mst')->where('name', 'in', array_keys($amount))->execute()->as_array();

            //create a insert array with the mapped fields for (address)
            $address_mapped_field = [];
            foreach ($map_address_field as $k => $v) {
                $address_mapped_field[$v['data_mapping_field']] = $address[$v['name']];
            }
            $address_mapped_field['main_category_id'] = $category['main'];
            $address_mapped_field['sub_category_id'] = $category['sub'];
            $address_mapped_field['created_at'] = Fame\Utils::timeNow();
            $address_mapped_field['updated_at'] = Fame\Utils::timeNow();
            $address_mapped_field['user_id'] = $this->user_id;

            //create a insert array with the mapped fields for (amount)
            $amount_mapped_field = [];
            $amount_mapped_field['user_id'] = $this->user_id;

            foreach ($map_amount_field as $k => $v) {
                $amount_mapped_field[$v['data_mapping_field']] = $amount[$v['name']];
            }

            //Trigger the insert to asset mst
            list($asset_id, $af) = DB::insert('assets_mst')->set($address_mapped_field)->execute();

            $amount_mapped_field['assets_id'] = $asset_id;
            $amount_mapped_field['created_at'] = Fame\Utils::timeNow();
            $amount_mapped_field['updated_at'] = Fame\Utils::timeNow();
            list($assets_amount_id, $af) = DB::insert('assets_amount_mst')->set($amount_mapped_field)->execute();


//            Fame\AssetsMst::insert();
            $r = [
                'status' => true,
                'data' => $asset_id,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    /*
     * edit assets
     * @deprecated
     */
    public function get_edit()
    {
        try {
            $data = [];
            $params = \Input::get();

            $id = $params['asset_id'];
            unset($params['token']);
            unset($params['asset_id']);
            unset($params['callback']);

            $params['updated_at'] = Fame\Utils::timeNow();
            $data = Fame\AssetsMst::update(['asset_id' => $id], $params);

            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    /**
     * delete the asset
     * @deprecated
     */
    public function get_delete()
    {
        try {
            $data = Fame\AssetsMst::remove(['asset_id' => Input::get('asset_id')]);
            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }


}