<?php

/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 1/25/2018
 * Time: 11:25 AM
 */
class Controller_Api_Sec_Field extends Controller_Api_Sec_Check
{

    public function get_index()
    {
        return $this->response(array(
            'status' => true,
//            'user_id' => $this->user_id,
            'version' => '1.0.0',
            'message' => 'Field controller . Test base api',
        ));
    }


    public function get_all()
    {
        try {

            $data = Fame\FieldMst::field_by_category(['category_id' => Input::get('category_id', 2), 'special_fld' => Input::get('special_fld', 0)]);
            $r = [
                'status' => ($data) ? true : false,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }
}