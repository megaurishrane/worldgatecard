<?php

/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 7/9/2017
 * Time: 4:55 PM
 */
class Controller_Api_Sec_Category extends Controller_Api_Sec_Check
{

    public function get_index()
    {
        return $this->response(array(
            'status' => true,
            'user_id' => $this->user_id,
            'version' => '1.0.0',
            'message' => 'Category controller . Test base api',
        ));
    }


    public function get_all()
    {
        try {
            $data = Fame\CategoryMst::get(['parent_id' => 0]);
            $tempData = [];

            foreach ($data as $key => $value) {
                array_push($tempData, $value);
                $subCategory = Fame\CategoryMst::get(['parent_id' => $value['category_id']]);
                if (isset($subCategory[0])) {
                    foreach ($subCategory as $k2 => $v2) {
                        array_push($tempData, $v2);
                    }
                }
            }

            $r = [
                'status' => true,
                'data' => $tempData,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    public function get_allParent()
    {
        try {
            $params = \Input::get();
            $data = Fame\CategoryMst::get(['type' => 0]);

            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    public function get_allSub()
    {
        try {
            $parent_category_id = \Input::get('parent_category_id', false);

            if ($parent_category_id) {
                $data = Fame\CategoryMst::get(['parent_id' => $parent_category_id]);
            } else {
                $data = Fame\CategoryMst::get(['type' => 1]);
            }


            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }
}