<?php
use Fuel\Core\Input;

class Controller_Api_Sec_Check extends \Fuel\Core\Controller_Rest
{

    public $user_id = false;

    /**
     * @var \Fame\Auth\Auth
     */
    public $auth_instance;

    public function before()
    {
        parent::before();

        \Fame\Auth\Auth::setAuthMethod(\Fame\Auth\Auth::auth_method_token);

        try {
            $user_id = \Fame\Auth\Auth::logged_in_user_id();
            if (!$user_id)
                throw new \Fame\Exception\UserException('Your login was expired');

            $this->user_id = $user_id;
            $this->auth_instance = \Fame\Auth\Auth::instance($this->user_id);
        } catch (Exception $e) {
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
            echo \Fuel\Core\Format::forge($r)->to_jsonp();
            die;
        }

        $this->format = 'jsonp';
    }
}
