<?php

/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 7/3/2017
 * Time: 4:32 PM
 */
class Controller_Api_Labs extends \Fuel\Core\Controller_Rest
{

    public function get_index()
    {
        return $this->response(array(
            'status' => true,
            'version' => '1.0.0',
            'message' => 'Labs controller . Test base api',
        ));
    }


    public function get_mysql()
    {
        try {

            $users = DB::select()
                ->from('SYSCompanyMast')
                ->execute('mssql')->as_array();

            $r = [
                'status' => true,
                'data' => 'some data',
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    public function get_template()
    {
        try {

            $r = [
                'status' => true,
                'data' => 'some data',
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    public function get_assets()
    {
        try {

            $data = Fame\AssetsMst::all_join([], null, false, 0, 'sub_category_id');
            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    /** get all user
     *#todo just for dummy purpose delete later
     */
    public function get_userlist()
    {
        try {
            $table = 'users';
            $data = DB::select()->from($table)->execute()->as_array();
            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }

    /** update the status */
    public function get_statechange()
    {
        try {
            $data = [];
            $table = 'users';
            $update = DB::update($table)->set([
                'account_active' => (Input::get('active_state') == 1) ? 0 : 1
            ])->where('id', Input::get('id'))->execute();

            $r = [
                'status' => true,
                'data' => $data,
            ];
        } catch (Exception $e) {
            $e = \Fame\Exception\ExceptionInterceptor::intercept($e);
            $r = [
                'status' => false,
                'reason' => $e->getMessage(),
            ];
        }

        $this->response($r);
    }
}
