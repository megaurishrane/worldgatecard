<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
    'default' => array(
        'connection'  => array(
            'dsn'        => 'mysql:host=localhost;dbname=worldgate_db',
            'username'   => 'root',
            'password'   => '',
        ),
    ),
    'mssql'=> array( 
    'type'           => 'pdo',
    'connection'     => array(
        'dsn'            => 'sqlsrv:host=FTS-CPU007\SQL2016;dbname=FameERALatest',
        'username'       => 'sa',
        'password'       => '$QL2016',
        'persistent'     => false,
        'compress'       => false,
    ),
    'identifier'     => '"',
    'table_prefix'   => '',
    'charset'        => 'utf8',
    'enable_cache'   => true,
    'profiling'      => false,)
);