<!DOCTYPE html>
<html lang="en">

<head>
    <title>Asset Album | App landing page</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php
    echo \Fuel\Core\Asset::css([
        'animate.css',
        'owl.carousel.css',
        'bootstrap.min.css',
        'font-awesome.min.css',
        'style-demo-3.css',
    ]);
    ?>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- header -->
    <header>
        <!-- navbar -->
        <nav class="navbar navbar-custom navbar-fixed-top" data-spy="affix" data-offset-top="100">
            <div class="container">
                <div class="row">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand page-scroll" href="#"><img alt="" src="assets/img/logo.png"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right menu">
                            <li>
                                <a class="active" href="#welcome">Welcome</a>
                            </li>
                            <li>
                                <a href="#service">Service</a>
                            </li>
                            <li>
                                <a href="#overview">Overview</a>
                            </li>
                            <li>
                                <a href="#features">features</a>
                            </li>
                            <li>
                                <a href="#team">team</a>
                            </li>
                            <li>
                                <a href="#pricing">pricing</a>
                            </li>
                            <li class="hidden-xs">
                                <a href="#download">download</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
            </div>
        </nav>
        <!-- End navbar -->
    </header>
    <!-- End header -->
    <!-- banner -->
    <div id="welcome" class="banner-content overly bg-parallax">
        <div id="banner-carousel" class="carousel slide" data-ride="carousel" data-interval="3000">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="container">
                        <div class="row">
                            <div class="intro-text col-md-7 col-sm-6">
                                <h1 class="intro-heading">Best WEBSITE for app showcase.</h1>
                                <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit repudiandae id. Quo
                                    ex alia natum tritani, dolorum persequeris Idque accusam ea sit repudiandae debet
                                    utinam.</p>
                                <div class="button">
                                    <a class="btn btn-primary" href="#">Download</a>
                                    <a class="btn btn-primary" href="#">Learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="mockup hidden-xs">
                        <img alt="" src="assets/img/mockup-iphone-2.png">
                    </div>
                </div>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="intro-text col-md-7 col-sm-6">
                                <h1 class="intro-heading">Now managment in your hand.</h1>
                                <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit repudiandae id. Quo
                                    ex alia natum tritani, dolorum persequeris Idque accusam ea sit repudiandae debet
                                    utinam.</p>
                                <div class="button">
                                    <a class="btn btn-primary" href="#">Download</a>
                                    <a class="btn btn-primary" href="#">Learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="mockup hidden-xs">
                        <img alt="" src="assets/img/mockup-iphone-2.png">
                    </div>
                </div>

                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="intro-text col-md-7 col-sm-6">
                                <h1 class="intro-heading">Experience a new inevention.</h1>
                                <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit repudiandae id. Quo
                                    ex alia natum tritani, dolorum persequeris Idque accusam ea sit repudiandae debet
                                    utinam.</p>
                                <div class="button">
                                    <a class="btn btn-primary" href="#">Download</a>
                                    <a class="btn btn-primary" href="#">Learn more</a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="mockup hidden-xs">
                                <img alt="" src="assets/img/mockup-iphone-2.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#banner-carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#banner-carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- End Banner -->

    <!-- Service -->
    <section id="service" class="service bg-grey sec-pad">
        <div class="container">
            <div class="row">
                <div class="service-holder">
                    <div class="ser-row text-center">
                        <div class="col-sm-6 col-md-3 service-colum">
                            <div class="service-icon">
                                <img alt="" src="assets/img/icon-2.png">
                            </div>
                            <div class="service-text">
                                <h4>Multiple Device</h4>
                                <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit, has justo
                                    adversarium. eu debet utinam vim. Idque accusam ea sit, has justo adversarium. </p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3 service-colum">
                            <div class="service-icon">
                                <img alt="" src="assets/img/icon-1.png">
                            </div>
                            <div class="service-text">
                                <h4>Powerful Data</h4>
                                <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit, has justo
                                    adversarium.
                                    eu debet utinam vim. Idque accusam ea sit, has justo adversarium </p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3 service-colum">
                            <div class="service-icon">
                                <img alt="" src="assets/img/icon-3.png">
                            </div>
                            <div class="service-text">
                                <h4>Unlimited Data</h4>
                                <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit, has justo
                                    adversarium. eu debet utinam vim. Idque accusam ea sit, has justo adversarium. </p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3 service-colum">
                            <div class="service-icon">
                                <img alt="" src="assets/img/icon-4.png">
                            </div>
                            <div class="service-text">
                                <h4>24/7 Support</h4>
                                <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit, has justo
                                    adversarium. eu debet utinam vim. Idque accusam ea sit, has justo adversarium. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Service -->

    <!-- About app -->
    <section id="overview" class="about-app sec-pad">
        <div class="container">
            <div class="row">
                <div class="overview-details row">
                    <div class="col-md-6 col-md-push-5 col-md-offset-1">
                        <div class="mockup-abt">
                            <img class="animated wow zoomIn" data-wow-delay="0.2s" alt="" src="assets/img/abt-mockup.png">
                        </div>
                    </div>
                    <div class="col-md-5 col-md-pull-7">
                        <div class="abt-text">
                            <h2>Best app for<br> <span>manage your task</span></h2>
                            <p>Lorem ipsum dolor sit amet, pro et case assentior, ne duo sint scribentur omittantur. Soluta
                                quaeque atomorum ad eos, mei ei euripidis adipiscing signiferumque.</p>
                            <a class="btn btn-secondary" href="#"><i class="fa fa-apple" aria-hidden="true"></i>App
                                store</a>
                            <a class="btn btn-secondary" href="#"><i class="fa fa-play" aria-hidden="true"></i>Google
                                play</a>
                        </div>
                    </div>
                </div>
                <div class="overview-details row">
                    <div class="col-md-6">
                        <div class="mockup-abt">
                            <img class="animated wow zoomIn" data-wow-delay="0.3s" alt="" src="assets/img/abt-mockup-2.png">
                        </div>
                    </div>
                    <div class="col-md-5 col-md-offset-1">
                        <div class="abt-text">
                            <h2>Now everything <br> <span>in your hand</span></h2>
                            <p>Lorem ipsum dolor sit amet, pro et case assentior, ne duo sint scribentur omittantur. Soluta
                                quaeque atomorum ad eos mei ei euripidis adipiscing signiferumque.</p>
                            <a class="btn btn-secondary" href="#">Get The App</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End about app -->

    <!-- Feature -->
    <section id="features" class="feature sec-pad">
        <div class="section-text text-center">
            <h1 class="section-heading">Amazing features</h1>
            <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit, has justo adversarium<br> repudiandae
                id. Quo ex alia natum tritani, </p>
        </div>
        <div class="mockup-flow">
            <img alt="" src="assets/img/mockup-flow.png">
        </div>
        <div class="feature-content overly sec-pad bg-parallax">
            <div class="container">
                <div class="row">
                    <div class="feature-holder clearfix">
                        <div class="col-md-4 col-sm-8 col-sm-offset-2 col-md-offset-0 left-feature">
                            <div class="feature-list">
                                <div class="icon">
                                    <i class="fa fa-leaf" aria-hidden="true"></i>
                                </div>
                                <div class="feature-text">
                                    <h3 class="feature-title">
                                        Creative
                                    </h3>
                                    <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut,
                                        feugiat inimicus voluptatibus te pri. Ad </p>
                                </div>
                            </div>
                            <div class="feature-list">
                                <div class="icon">
                                    <i class="fa fa-cube" aria-hidden="true"></i>
                                </div>
                                <div class="feature-text">
                                    <h3 class="feature-title">
                                        Modern
                                    </h3>
                                    <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut,
                                        feugiat inimicus voluptatibus te pri. Ad </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-8 col-sm-offset-2 col-md-offset-0 right-feature">
                            <div class="feature-list wow">
                                <div class="icon">
                                    <i class="fa fa-rocket" aria-hidden="true"></i>
                                </div>
                                <div class="feature-text">
                                    <h3 class="feature-title">
                                        Responsive
                                    </h3>
                                    <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut,
                                        feugiat inimicus voluptatibus te pri. Ad </p>
                                </div>
                            </div>
                            <div class="feature-list">
                                <div class="icon">
                                    <i class="fa fa-cogs" aria-hidden="true"></i>
                                </div>
                                <div class="feature-text">
                                    <h3 class="feature-title">
                                        User friendly
                                    </h3>
                                    <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut,
                                        feugiat inimicus voluptatibus te pri. Ad </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-8 col-sm-offset-2 col-md-offset-0 right-feature">
                            <div class="feature-list">
                                <div class="icon">
                                    <i class="fa fa-rocket" aria-hidden="true"></i>
                                </div>
                                <div class="feature-text">
                                    <h3 class="feature-title">
                                        Clean Design
                                    </h3>
                                    <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut,
                                        feugiat inimicus voluptatibus te pri. Ad </p>
                                </div>
                            </div>
                            <div class="feature-list">
                                <div class="icon">
                                    <i class="fa fa-cogs" aria-hidden="true"></i>
                                </div>
                                <div class="feature-text">
                                    <h3 class="feature-title">
                                        Multi Tasking
                                    </h3>
                                    <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut,
                                        feugiat inimicus voluptatibus te pri. Ad </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Feature -->

    <!-- Feature -->
    <section id="specification" class="specification sec-pad">
        <div class="container">
            <div class="row">
                <div class="section-text text-center">
                    <h1 class="section-heading">why this app is best</h1>
                    <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit, has justo adversarium<br>
                        repudiandae id. Quo ex alia natum tritani, </p>
                </div>
                <div class="specification-holder clearfix">
                    <div class="col-md-4 col-sm-8 col-sm-offset-2 col-md-offset-0 left-specification">
                        <div class="specification-list  wow fadeInLeft" data-wow-delay=".1s">
                            <div class="icon">
                                <i class="fa fa-leaf" aria-hidden="true"></i>
                            </div>
                            <div class="specification-text">
                                <div class="specification-title">
                                    Creative
                                </div>
                                <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut, feugiat
                                    inimicus voluptatibus te pri. Ad </p>
                            </div>
                        </div>
                        <div class="specification-list  wow fadeInLeft" data-wow-delay=".1s">
                            <div class="icon">
                                <i class="fa fa-cube" aria-hidden="true"></i>
                            </div>
                            <div class="specification-text">
                                <div class="specification-title">
                                    Simple to Use
                                </div>
                                <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut, feugiat
                                    inimicus voluptatibus te pri. Ad </p>
                            </div>
                        </div>
                        <div class="specification-list  wow fadeInLeft" data-wow-delay=".1s">
                            <div class="icon">
                                <i class="fa fa-cube" aria-hidden="true"></i>
                            </div>
                            <div class="specification-text">
                                <div class="specification-title">
                                    Clean
                                </div>
                                <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut, feugiat
                                    inimicus voluptatibus te pri. Ad </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-8 col-sm-offset-2 col-md-offset-0">
                        <div class="specification-mockup wow animated pulse" data-wow-delay=".5s">
                            <img alt="" src="assets/img/feature-mockup-iphone.png">
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-8 col-sm-offset-2 col-md-offset-0 right-specification">
                        <div class="specification-list wow fadeInRight" data-wow-delay=".1s">
                            <div class="icon">
                                <i class="fa fa-rocket" aria-hidden="true"></i>
                            </div>
                            <div class="specification-text">
                                <div class="specification-title">
                                    Powerful
                                </div>
                                <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut, feugiat
                                    inimicus voluptatibus te pri. Ad </p>
                            </div>
                        </div>
                        <div class="specification-list wow fadeInRight" data-wow-delay=".1s">
                            <div class="icon">
                                <i class="fa fa-cogs" aria-hidden="true"></i>
                            </div>
                            <div class="specification-text">
                                <div class="specification-title">
                                    Light version
                                </div>
                                <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut, feugiat
                                    inimicus voluptatibus te pri. Ad </p>
                            </div>
                        </div>
                        <div class="specification-list  wow fadeInRight" data-wow-delay=".1s">
                            <div class="icon">
                                <i class="fa fa-cube" aria-hidden="true"></i>
                            </div>
                            <div class="specification-text">
                                <div class="specification-title">
                                    User friendly
                                </div>
                                <p>Utinam laudem mea ei. Est ex cotidieque delicatissimi. Doctus praesent usu ut, feugiat
                                    inimicus voluptatibus te pri. Ad </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Feature -->

    <!-- Video -->
    <!--<section class="video overly sec-pad">-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <button type="button" class="btn-video" data-toggle="modal" data-target="#myModal">-->
    <!--                <i class="fa fa-play-circle" aria-hidden="true"></i>-->
    <!--            </button>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- End Video -->

    <!-- Screenshot -->
    <section class="screenshot sec-pad">
        <div class="container">
            <div class="row">
                <div class="section-text text-center">
                    <h1 class="section-heading">apps screenshot</h1>
                    <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit, has justo adversarium<br>
                        repudiandae id. Quo ex alia natum tritani.</p>
                </div>
                <div class="owl-carousel loop nplr screen-loop">
                    <div>
                        <img alt="" src="assets/img/screenshot-img-1.jpg">
                    </div>
                    <div>
                        <img alt="" src="assets/img/screenshot-img-2.jpg">
                    </div>
                    <div>
                        <img alt="" src="assets/img/screenshot-img-3.jpg">
                    </div>
                    <div>
                        <img alt="" src="assets/img/screenshot-img-4.jpg">
                    </div>
                    <div>
                        <img alt="" src="assets/img/screenshot-img-2.jpg">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Screenshot -->

    <!-- Team -->
    <section id="team" class="team-member bg-grey sec-pad">
        <div class="container">
            <div class="row">
                <div class="section-text text-center">
                    <h1 class="section-heading">innovative team</h1>
                    <p>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit, has justo adversarium<br>
                        repudiandae id. Quo ex alia natum tritani,</p>
                </div>
                <div class="col-md-12 nplr">
                    <div class="team-member-profile">
                        <div class="col-md-3 col-sm-6">
                            <div class="team-mem-col res-margin">
                                <a href="#" class="link">
                                    <div class="team-hover">
                                        <div class="team-hover-content text-center">
                                            <div class="extra-link">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                            </div>
                                            <p>Mazim rationibus in sea. Harum sententiae eu eos, usu habemus periculis id,
                                                quo dican</p>
                                        </div>
                                    </div>
                                    <div class="image">
                                        <img alt="" src="assets/img/team-member.jpg">
                                    </div>
                                </a>
                                <div class="member-name text-center">
                                    <h4>Mark Linux</h4>
                                    <p>UI/UX Designer</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="team-mem-col res-margin">
                                <a href="#" class="link">
                                    <div class="team-hover">
                                        <div class="team-hover-content text-center">
                                            <div class="extra-link">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                            </div>
                                            <p>Mazim rationibus in sea. Harum sententiae eu eos, usu habemus periculis id,
                                                quo dican</p>
                                        </div>
                                    </div>
                                    <div class="image">
                                        <img alt="" src="assets/img/team-member-2.jpg">
                                    </div>
                                </a>
                                <div class="member-name text-center">
                                    <h4>Maria Hedge</h4>
                                    <p>UI/UX Designer</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="team-mem-col res-margin">
                                <a href="#" class="link">
                                    <div class="team-hover">
                                        <div class="team-hover-content text-center">
                                            <div class="extra-link">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                            </div>
                                            <p>Mazim rationibus in sea. Harum sententiae eu eos, usu habemus periculis id,
                                                quo dican</p>
                                        </div>
                                    </div>
                                    <div class="image">
                                        <img alt="" src="assets/img/team-member-3.jpg">
                                    </div>
                                </a>
                                <div class="member-name text-center">
                                    <h4>David Flecher</h4>
                                    <p>UI/UX Designer</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="team-mem-col">
                                <a href="#" class="link">
                                    <div class="team-hover">
                                        <div class="team-hover-content text-center">
                                            <div class="extra-link">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                            </div>
                                            <p>Mazim rationibus in sea. Harum sententiae eu eos, usu habemus periculis id,
                                                quo dican</p>
                                        </div>
                                    </div>
                                    <div class="image">
                                        <img alt="" src="assets/img/team-member-4.jpg">
                                    </div>
                                </a>
                                <div class="member-name text-center">
                                    <h4>Riana liza</h4>
                                    <p>UI/UX Designer</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Team -->

    <!-- Testimonial -->
    <section class="testimonial sec-pad">
        <div class="owl-carousel loop-testi">
            <div>
                <div class="testimonial-wrapper">
                    <img alt="" src="assets/img/profile-img.jpg">
                    <h4>Mark Jhonson</h4>
                    <h5>CEO, Ark foundation</h5>
                    <p><i>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit repudiandae id. Quo ex alia
                            natum tritani, dolorum persequeris.</i></p>
                </div>
            </div>
            <div>
                <div class="testimonial-wrapper">
                    <img alt="" src="assets/img/profile-img.jpg">
                    <h4>Mark Jhonson</h4>
                    <h5>CEO, Ark foundation</h5>
                    <p><i>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit repudiandae id. Quo ex alia
                            natum tritani, dolorum persequeris.</i></p>
                </div>
            </div>
            <div>
                <div class="testimonial-wrapper">
                    <img alt="" src="assets/img/profile-img.jpg">
                    <h4>Mark Jhonson</h4>
                    <h5>CEO, Ark foundation</h5>
                    <p><i>Lorem ipsum dolor sit amet, eu debet utinam vim. Idque accusam ea sit repudiandae id. Quo ex alia
                            natum tritani, dolorum persequeris.</i></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End testimonial -->


    <!-- download-section -->
    <section id="download" class="download bg-gradient sec-pad">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="download-text">
                        <h1>Available on all platform</h1>
                        <p>Lorem ipsum dolor sit amet eu debet utinam vim. Idque accusam ea sit has justo adversarium<br>
                            repudiandae id. Quo ex alia natum tritani Idque accusam ea sit has justo</p>
                        <div class="button">
                            <a class="btn btn-primary" href="#"><i class="fa fa-apple" aria-hidden="true"></i>App store</a>
                            <a class="btn btn-primary" href="#"><i class="fa fa-play" aria-hidden="true"></i>Google play</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End download-section -->

    <!-- Pricing -->
    <section id="pricing" class="pricing sec-pad">
        <div class="container">
            <div class="row">
                <div class="section-text text-center">
                    <h1 class="section-heading">our pricing plans</h1>
                    <p>Lorem ipsum dolor sit amet, menandri lobortis laboramus nec ex, ullum regione instructior duo ei.
                        Hinc facilisi dignissim<br> vel eu. Mei quem denique dissentiunt at, usu omnium </p>
                </div>
                <div class="nplr pricing-wrapper">
                    <div class="col-sm-6 col-md-3">
                        <div class="pricing-table animated res-margin wow bounceIn" data-wow-delay="0.1s">
                            <div class="pricing-header">
                                <h4>INITIAL</h4>
                            </div>
                            <div class="pricing-content">
                                <div class="main-pricing">
                                    <h4>$20</h4>
                                    <p>Per Month</p>
                                </div>
                                <div class="pricing-feature">
                                    <ul>
                                        <li>10 gb free space</li>
                                        <li>20 gb free storage</li>
                                        <li>full support</li>
                                        <li>unimited download</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pricing-footer">
                                <a class="btn btn-secondary" href="#">Get Started</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="pricing-table res-margin animated wow bounceIn" data-wow-delay="0.2s">
                            <div class="pricing-header">
                                <h4>STANDERD</h4>
                            </div>
                            <div class="pricing-content">
                                <div class="main-pricing">
                                    <h4>$30</h4>
                                    <p>Per Month</p>
                                </div>
                                <div class="pricing-feature">
                                    <ul>
                                        <li>10 gb free space</li>
                                        <li>20 gb free storage</li>
                                        <li>full support</li>
                                        <li>unimited download</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pricing-footer">
                                <a class="btn btn-secondary" href="#">Get Started</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="pricing-table res-margin clearfix animated wow bounceIn" data-wow-delay="0.3s">
                            <div class="pricing-header">
                                <h4>BUSINESS</h4>
                            </div>
                            <div class="pricing-content">
                                <div class="main-pricing">
                                    <h4>$40</h4>
                                    <p>Per Month</p>
                                </div>
                                <div class="pricing-feature">
                                    <ul>
                                        <li>10 gb free space</li>
                                        <li>20 gb free storage</li>
                                        <li>full support</li>
                                        <li>unimited download</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pricing-footer">
                                <a class="btn btn-secondary" href="#">Get Started</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="pricing-table animated wow bounceIn" data-wow-delay="0.4s">
                            <div class="pricing-header">
                                <h4>ULTIMATE</h4>
                            </div>
                            <div class="pricing-content">
                                <div class="main-pricing">
                                    <h4>$50</h4>
                                    <p>Per Month</p>
                                </div>
                                <div class="pricing-feature">
                                    <ul>
                                        <li>10 gb free space</li>
                                        <li>20 gb free storage</li>
                                        <li>full support</li>
                                        <li>unimited download</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="pricing-footer">
                                <a class="btn btn-secondary" href="#">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Pricing -->

    <!-- Faq section -->
    <section class="faq-sec bg-grey sec-pad">
        <div class="container demo">
            <div class="row">
                <div class="section-text text-center">
                    <h1 class="section-heading">frequently asked questions</h1>
                    <p>Lorem ipsum dolor sit amet, menandri lobortis laboramus nec ex, ullum regione instructior duo ei.
                        Hinc facilisi dignissim<br> vel eu. Mei quem denique dissentiunt at, usu omnium </p>
                </div>
                <div class="col-md-4">
                    <img alt="" src="assets/img/faq-image.png">
                </div>
                <div class="panel-group col-md-8" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                    What is Appexo?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                                single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                                beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                                lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                                probably haven't heard of them accusamus labore sustainable VHS.
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                    squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                    quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it
                                    squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,
                                    craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
                                    butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                                    nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-minus"></i>
                                    How it works?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                                single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                                beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                                lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                                probably haven't heard of them accusamus labore sustainable VHS.
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                    squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                    quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it
                                    squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,
                                    craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
                                    butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                                    nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                    How to use?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                                single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                                beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                                lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                                probably haven't heard of them accusamus labore sustainable VHS.
                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                                    squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck
                                    quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it
                                    squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,
                                    craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
                                    butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth
                                    nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                            </div>
                        </div>
                    </div>
                </div><!-- panel-group -->
            </div>
        </div>
    </section>
    <!-- End faq -->

    <!--Blog  -->
    <section id="blog" class="blog sec-pad">
        <div class="container">
            <div class="row">
                <div class="section-text text-center">
                    <h1 class="section-heading">Latest blog</h1>
                    <p>Lorem ipsum dolor sit amet, menandri lobortis laboramus nec ex, ullum regione instructior duo ei.
                        Hinc facilisi dignissim<br> vel eu. Mei quem denique dissentiunt at, usu omnium </p>
                </div>
                <div class="col-md-4 res-margin">
                    <div class="blog-col">
                        <a href="#"><img alt="" src="assets/img/blog-img-1.jpg"></a>
                        <ul class="blog-about">
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i><a href="#">mike alert</a>
                            </li>
                            <li>
                                <i class="fa fa-clock-o" aria-hidden="true"></i><a href="#">Janu. 15 2017</a>
                            </li>
                            <li>
                                <i class="fa fa-comments" aria-hidden="true"></i><a href="#">20 Comments</a>
                            </li>
                        </ul>
                        <div class="blog-text">
                            <h4><a href="#">Clean app forever</a></h4>
                            <p>Lorem ipsum dolor sit amet, ridens eligendi an nec, his nostro dolorum splendide te. Docendi
                                intellegebat eu pro, ius in facilis reprimique. Primis aliquando vis ne. At per diceret
                                numquam. Ne vim aliquid accusamus.</p>
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 res-margin">
                    <div class="blog-col">
                        <a href="#"><img alt="" src="assets/img/blog-img-2.jpg"></a>
                        <ul class="blog-about">
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i><a href="#">mike alert</a>
                            </li>
                            <li>
                                <i class="fa fa-clock-o" aria-hidden="true"></i><a href="#">Janu. 15 2017</a>
                            </li>
                            <li>
                                <i class="fa fa-comments" aria-hidden="true"></i><a href="#">20 Comments</a>
                            </li>
                        </ul>
                        <div class="blog-text">
                            <h4><a href="#">Best design app for you</a></h4>
                            <p>Lorem ipsum dolor sit amet, ridens eligendi an nec, his nostro dolorum splendide te. Docendi
                                intellegebat eu pro, ius in facilis reprimique. Primis aliquando vis ne. At per diceret
                                numquam. Ne vim aliquid accusamus.</p>
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="blog-col">
                        <a href="#"><img alt="" src="assets/img/blog-img-3.jpg"></a>
                        <ul class="blog-about">
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i><a href="#">mike alert</a>
                            </li>
                            <li>
                                <i class="fa fa-clock-o" aria-hidden="true"></i><a href="#">Janu. 15 2017</a>
                            </li>
                            <li>
                                <i class="fa fa-comments" aria-hidden="true"></i><a href="#">20 Comments</a>
                            </li>
                        </ul>
                        <div class="blog-text">
                            <h4><a href="#">Better app for better world</a></h4>
                            <p>Lorem ipsum dolor sit amet, ridens eligendi an nec, his nostro dolorum splendide te. Docendi
                                intellegebat eu pro, ius in facilis reprimique. Primis aliquando vis ne. At per diceret
                                numquam. Ne vim aliquid accusamus.</p>
                            <a href="#">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End blog -->

    <!-- Subscribe -->
    <section class="subscribe sec-pad">
        <div class="container">
            <div class="row">
                <div class="section-text text-center animated wow fadeInUp" data-wow-delay=".1s">
                    <h1 class="section-heading">subscribe now</h1>
                    <p>Lorem ipsum dolor sit amet, menandri lobortis laboramus nec ex, ullum regione instructior duo ei.
                        Hinc facilisi dignissim<br> vel eu. Mei quem denique dissentiunt at, usu omnium </p>
                </div>
                <form>
                    <div class="form-group col-md-8 col-md-offset-2 custom-form animated wow fadeInUp" data-wow-delay=".1s">
                        <label for="email"><i class="fa fa-envelope-o" aria-hidden="true"></i></label>
                        <input type="email" class="form-control" id="email">
                        <button>Subscribe</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- End Subscribe -->
    <!-- Map -->
    <section class="map-holder">
        <div id="gmap"></div>
    </section>
    <!-- End map -->
    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="contact-holder row">
                    <div class="col-md-6 res-margin">
                        <h3>Get in Touch</h3>
                        <form class="custom-form">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Messege"></textarea>
                            </div>
                            <button>SEND</button>
                        </form>
                    </div>
                    <div class="col-md-5 col-md-offset-1">
                        <h3>About us</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis ab quia officia, minus
                            obcaecati corporis! Tenetur tempore, inventore cum sapiente minima accusantium illo animi,
                            doloribus rerum deleniti cumque, consequatur eaque in unde facilis consectetur, eius eligendi
                            nostrum. Facilis, recusandae, eos!</p>
                        <ul class="contact-details">
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span>+973 17 217 991</span>
                            </li>
                            <li><i class="fa fa-phone" aria-hidden="true"></i>
                                <span>Suite No. 122, 12th floor, Al Zamil Towers, Manama, Kingdom of Bahrain </span>
                            </li>
                            <li><i class="fa fa-envelope" aria-hidden="true"></i>
                                <span>sales@fameERP.com</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Footer  -->
    <footer class="footer-wrapper">
        <div class="container">
            <div class="row">
                <div class="social-holder">
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
                </div>
                <div class="copyright">
                    2017 COPYRIGHT <strong>FameERP</strong> . ALL RIGHTS RESERVED
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- MODAL video -->
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog custom-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="http://www.youtube.com/embed/qaOvHKG0Tio?html5=1"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal video -->

    <!-- Google Map Script -->
    <!--<script type="text/javascript"-->
    <!-- src="https://maps.google.com/maps/api/js?key=AIzaSyDNaa8LOQISEST6NIGC78vmdnIOynR2SeU"></script>-->

    <!--<script src="js/gmaps.js"></script>-->
    <?php
    echo \Fuel\Core\Asset::js([
        'gmaps.js',
        'jquery.min.js',
        'bootstrap.js',
        'owl.carousel.min.js',
        'wow.js',
        'script.js'
    ]);
    ?>
</body>

</html>