<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php

    use Fuel\Core\Uri;

    echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
    <script src="https://secure-egypt.paytabs.com/payment/js/paylib.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.4/bluebird.min.js"></script>
    <script src="https://secure.gosell.io/js/sdk/tap.min.js"></script>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>



    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!--Header & Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Header & Navigation -->

    <!--End HEADER SECTION-->

    <section>
        <div class="tr-register">
            <div class="tr-regi-form">
                <h4>Enter Amount in BHD</h4>
                <!--				<p>It's free and always will be.</p>-->

                <form class="col s12" id="payform" action="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/paywtab'; ?>" method="post">
                    <!-- 
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" name="full_name" class="validate">
                            <label>Full Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="email" name="email_address" class="validate">
                            <label>Email Id</label>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="number" name="cart_amount" class="validate">
                            <label>Amount</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" class="validate" name="cart_currency" value="EGP" readonly>
                            <!-- <label>BHD</label> -->
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" name="profile_id" value="83901" readonly>
                        <input type="hidden" name="tran_type" value="sale" readonly>
                        <input type="hidden" name="tran_class" value="trav" readonly>
                        <input type="hidden" name="cart_description" value="Membership Fee" readonly>
                        <input type="hidden" name="cart_id" value="<?php echo rand(1, 1000000); ?>" readonly>
                        <input type="hidden" name="callback" value="<?php echo Fuel\Core\Uri::base(false) . 'welcome/paytab/success'; ?>" readonly>
                        <input type="hidden" name="return" value="<?php echo Fuel\Core\Uri::base(false) . 'welcome/paytab/fail'; ?>" readonly>

                        <div class="input-field col s12">
                            <i class="waves-effect waves-light btn-large full-btn waves-input-wrapper" style="">
                                <input type="submit" value="Pay with Paytab" class="waves-button-input"></i>
                        </div>
                    </div>
                </form>


                <!--				<div class="soc-login">-->
                <!--					<h4>Sign in using</h4>-->
                <!--					<ul>-->
                <!--						<li><a href="#"><i class="fa fa-facebook fb1"></i> Facebook</a> </li>-->
                <!--						<li><a href="#"><i class="fa fa-twitter tw1"></i> Twitter</a> </li>-->
                <!--						<li><a href="#"><i class="fa fa-google-plus gp1"></i> Google</a> </li>-->
                <!--					</ul>-->
                <!--				</div>-->
            </div>
        </div>
    </section>
    <style>
        .error-message {
            background-color: #ff7c7c;
            border-radius: 6px;
            font-size: 11px;
            min-height: 37px;
            padding-top: 6px;
        }
    </style>
    <!--END DASHBOARD-->

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <section>
        <div class="rows tips tips-home tb-space home_title">
            <div class="container tips_1">
                <!-- TIPS BEFORE TRAVEL -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h3>Tips Before Travel</h3>
                    <div class="tips_left tips_left_1">
                        <h5>Bring copies of your passport</h5>
                        <p>It is good idea to keep a copy of your passport will traveling outside your own country.</p>
                    </div>
                    <div class="tips_left tips_left_2">
                        <h5>Register with your embassy</h5>
                        <p>Let your embassy know your are traveling in country so in any emergency will find your full.
                        </p>
                    </div>
                    <div class="tips_left tips_left_3">
                        <h5>Always have local cash</h5>
                        <p>Get your currency exchange before traveling at correct rate help will buying things. </p>
                    </div>
                </div>
                <!-- CUSTOMER TESTIMONIALS -->
                <div class="col-md-8 col-sm-6 col-xs-12 testi-2">
                    <!-- TESTIMONIAL TITLE -->
                    <h3>Customer Testimonials</h3>
                    <div class="testi">
                        <h4>Gaurish Naresh Rane</h4>
                        <p>Amazing experience with World Gate got a really good deal on hotel and flight travel. </p>
                        <address>Manama, Bahrain</address>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>


    <div class="hiddendiv common"></div>
</body>

</html>