<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!-- Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Navigation -->

    <!-- END HEADER SECTION -->

    <!--====== Benfits with World gate member ==========-->
    <section>
        <div class="rows inner_banner inner_banner_1" style="padding-top: 210px;">
            <div class="container">
                <h2>Member Benefits</h2>
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                    </li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="#" class="bread-acti">Member Benefits</a></li>
                </ul>
                <p>Join and enjoy fabulous room rates and many more facilities with us.</p>
            </div>
        </div>
    </section>
    <!--====== ALL POST ==========-->
    <section>
        <div class="rows inn-page-bg com-colo">
            <div class="container inn-page-con-bg tb-space pad-bot-redu-5" id="inner-page-title">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2>World Gate <span>Member</span> Program</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>The World Gate invites you to join and enjoy fabulous room rates and many more facilities with
                        us.
                        With amazing complimentary vouchers, being a world gate member now!</p>
                </div>
                <!--===== POSTS ======-->
                <div class="rows">
                    <div class="posts">

                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <br>
                            <h3 style="color:#AD974F;">Choose your Bahrain options</h3>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits/classic'; ?>" class="link-btn">Classic Membership (245 BHD)</a> <br><br>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits/black'; ?>" class="link-btn" style="width: 262px">Black Membership (490 BHD)</a><br><br>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits/vip'; ?>" class="link-btn" style="width: 262px">VIP Membership (735 BHD)</a>

                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <br>
                            <h3 style="color:#AD974F;">Choose your Saudi options</h3>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits_saudi/classic'; ?>" class="link-btn">Classic Membership (5000 SAR)</a> <br><br>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits_saudi/black'; ?>" class="link-btn" style="width: 262px">Black Membership (10000 SAR)</a><br><br>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits_saudi/vip'; ?>" class="link-btn" style="width: 262px">VIP Membership (15000 SAR)</a>

                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <br>
                            <h3 style="color:#AD974F;">Choose your Egypt options</h3>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits_egypt/classic'; ?>" class="link-btn">Classic Membership (5000 EGP)</a> <br><br>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits_egypt/black'; ?>" class="link-btn" style="width: 262px">Black Membership (10000 EGP)</a><br><br>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits_egypt/vip'; ?>" class="link-btn" style="width: 262px">VIP Membership (15000 EGP)</a>

                        </div>
                    </div>
                </div>
                <!--===== POST END ======-->
            </div>
        </div>
    </section>

    <style>
        .desc-p {

            font-style: normal !important;
            font-weight: 500 !important;
            margin-bottom: 20px !important;
        }
    </style>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/customer-testimonial'); ?>


    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>
