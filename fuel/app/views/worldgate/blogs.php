<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!--Header & Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Header & Navigation -->

    <!--End HEADER SECTION-->

    <!--====== BANNER ==========-->
    <section>
        <div class="rows inner_banner inner_banner_1">
            <div class="container">
                <h2> Blog</h2>
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                    </li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="#" class="bread-acti">Blog</a></li>
                </ul>
                <p>Know what new around the world with our latest blog.</p>
            </div>
        </div>
    </section>

    <!--====== ALL POST ==========-->
    <section>
        <div class="rows inn-page-bg com-colo">
            <div class="container inn-page-con-bg tb-space pad-bot-redu-5" id="inner-page-title">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2>Holiday Tour <span>Blog</span> Posts</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>World's leading tour and travels Booking website,Over 30,000 packages worldwide. Book travel
                        packages
                        and enjoy your holidays with distinctive experience</p>
                </div>
                <!--===== POSTS ======-->
                <div class="rows">
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/f264da728f9282e80ca440800ca881a1.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>Top 10 best luxury hotels in the world</h3>
                            <p>The question I am asked the most is the simplest: “what is your favorite hotel in the
                                world”. It’s somewhat difficult to answer that question, since I prefer to rank hotels
                                in different categories according to the experience they offer, whether it be a city
                                break, a romantic getaway, a wilderness experience or a beach holiday. Nevertheless,
                                there are a few luxury hotels that left such an overwhelming impression, that I rank
                                them amongst my most memorable experiences. So, here you go: this is my

                                10. ONE&amp;ONLY CAPE TOWN, SOUTH AFRICA |
                                9. NORTH ISLAND, SEYCHELLES |
                                8. CLAYOQUOT WILDERNESS RESORT, CANADA |
                                7. ALILA VILLAS ULUWATU, BALI, INDONESIA |
                                6. JADE MOUNTAIN, ST LUCIA |
                                5. PARK HYATT SYDNEY, AUSTRALIA |
                                4. IVORY LODGE LION SANDS, SOUTH AFRICA |
                                3. SONEVA FUSHI, MALDIVES |
                                2. AMANGIRI, UTAH, USA |
                                1. SONEVA KIRI, KOH KOOD, THAILAND |
                            </p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/8f0ff10fa37f76428a38b5be9ded3c43.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>ONE&amp;ONLY CAPE TOWN, SOUTH AFRICA</h3>
                            <p>This urban chic resort, located in Cape Town’s Waterfront, set a whole new standard for
                                South Africa hotels when it opened in 2009. Offering an unprecedented level of luxury
                                and style to one of the world’s most fascinating cities, the hotel will overwhelm you
                                from the very start of your stay with breathtaking Table Mountain views from the
                                spectacular lobby. The spacious room and oversized bathroom interiors are adorned with
                                fabrics and furnishings recalling the warm, muted tones of Africa’s savannahs. Central
                                to exciting nightlife and entertainment, the hotel serves as a gateway to the Cape
                                Town’s must visit places, such wine country excursions, Chapman’s Peak and Cape Point.
                            </p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/c058ece75426434ef6b37e589644489b.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>NORTH ISLAND, SEYCHELLES</h3>
                            <p>The private island resort has only 11 opulent hand-crafted villas overlooking the
                                pristine, powder-white sands and turquoise waters of the Ile Du Nord eco-reserve. Raised
                                off the ground to catch the cooling Indian Ocean breezes, all villas open up onto a
                                private garden and the beach, with plunge pools and outdoor showers providing the
                                finishing touches. The island’s lounge and dining areas, scenically located health spa
                                and gym, library and dive centre, and a rim-flow swimming pool are all built into a
                                granitic outcrop. Keep an eye out for Brutus, the island’s 160-year-old tortoise. The
                                resort was the decor for honeymoon of Prince William and the Duchess of Cambridge in
                                2011.</p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/3587d2fea4f3a605969ab8d66bfdfe08.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>CLAYOQUOT WILDERNESS RESORT, CANADA</h3>
                            <p>Clayoquot Wilderness Resort, open from May to September, is an exotic paradox of
                                indulgent luxury and remote, untamed wilderness, located in the Clayoquot Sound World
                                Biosphere Reserve (within Vancouver Island’s Pacific Rim National Park). This unique
                                21st-century-safari-style enclave offers exquisite cuisine and breathtaking adventures,
                                giving visitors a rare taste of how the fortunate spent their summers ‘roughing it’ some
                                100 years ago. Twenty white canvas suite tents showcase antique furnishings, opulent
                                rugs, down duvets, remote-controlled propane powered woodstoves and luxurious amenities.
                                Keep an eye out for the amazing wildlife, including bears and killer whales.</p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/4ba0e5341b32b8b9b9419cfe345252e5.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>ALILA VILLAS ULUWATU, BALI, INDONESIA</h3>
                            <p>Alila Villas Uluwatu is my favorite luxury resort in Bali’s. The ultraluxurious property
                                is located in the dry savannah landscape of the Bukit Peninsular in the South of the
                                island. Poised on an elevated plateau that meets with limestone cliffs sweeping down the
                                Indian Ocean, the view from the hotel is nothing less than breathtaking. Blending in
                                with these spectacular surroundings, the unique design and open-plan architecture of the
                                hotel combines the delights of traditional Balinese pavilion architecture and rural
                                landscapes with modern dynamic treatment of space and form. The spectacular rooms with
                                minimalistic interiors are designed to impart comfort and space.</p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/db9579ae5a6888969a82755fc77e977c.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>JADE MOUNTAIN, ST LUCIA</h3>
                            <p>Jade Mountain delivers romance in a very unique way from its hilltop location in a
                                verdant corner of the tropical Caribbean island of St Lucia. The impressive design, with
                                individual bridges leading to exceptional infinity pool suites and rugged stoned-faced
                                columns reaching towards the sky, makes Jade Mountain one of the Caribbean’s most
                                mesmerizing resort experiences, especially for honeymooners. With the fourth wall
                                entirely absent, Jade Mountain’s 29 suites (called “sanctuaries”) are stage-like
                                settings from which to embrace the full glory of St Lucia’s twin peaks, the Pitons World
                                Heritage Site, and of course, the eternal Caribbean Sea.</p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/2f446f1da279d7b8fb2ff24ae7db9128.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>PARK HYATT SYDNEY, AUSTRALIA</h3>
                            <p>Seated majestically on one of the world’s most beautiful harbours, Park Hyatt Sydney
                                perfectly personifies contemporary harbourside luxury with its coveted location between
                                the iconic Sydney Opera House and Harbour Bridge. Reminiscent of an exclusive
                                harbourside residence, the hotel offers intimate surrounds with architecture, art and
                                design that reflect the Australian landscape. Highlights include 155 spacious guestrooms
                                and suites with contemporary interiors and floor-to-ceiling glass doors that open to
                                private balconies; 24-hour butler service; and a day spa with stunning rooftop pool and
                                sundeck.</p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/845a186acf40fa61801e829ebf1052ff.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>IVORY LODGE LION SANDS, SOUTH AFRICA</h3>
                            <p>Experience the African Bushveld in lavish comfort and style like only few will.
                                “Unparalleled and extraordinarily beautiful in its simplicity and texture” is how Ivory
                                Lodge has been described, and it has been recognised with various international awards.
                                Each one of the lodge’s villas is surrounded by untamed African Bush and they all
                                include a private rim flow pool, exquisitely appointed large dedicated bedroom and
                                lounge, and simply more of everything. For the ultimate African bush experience, you may
                                want to stay at one of the ultraluxurious treehouses operated by the same management of
                                Ivory Lodge in the same wildlife reserve (where your chances of spotting the Big Five
                                are very high).</p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/fcd949ae6453e31c9242f499edadd563.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3> SONEVA FUSHI, MALDIVES</h3>
                            <p>Soneva Fushi Resort, the Maldives’ original desert island hideaway, is located in the
                                stunnnig Baa Atoll island archipelago, a UNESCO World Biosphere Reserve. Massive,
                                Robinson Crusoe-style, multi-bedroom luxury villas and private residences are hidden
                                among dense tropical foliage with views of the ridiculously clear lagoon. In keeping
                                with the castaway theme, many are built to resemble tree houses. All villas open to
                                their own private stretch of sugar white sands, and most boast their own private
                                seawater swimming pools. Intuitive service is provided by Mr./Ms. Friday butlers who
                                know what you want before you want it.</p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/ad68f437dd6b1a9c823b4b7d7237c907.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>AMANGIRI, UTAH, USA</h3>
                            <p>Amangiri (peaceful mountain) is located on 600 acres in Canyon Point, Southern Utah,
                                close to the border with Arizona. The resort is tucked into a protected valley with
                                sweeping views towards the Grand Staircase – Escalante National Monument. Built around a
                                central swimming pool with spectacular views, the resort blends into its dramatic
                                surrounds where deep canyons and towering plateaus create a raw landscape of immense
                                power. Amangiri’s trail system and unrivalled backcountry access allow guests to explore
                                the region’s striking desert scenery on foot. Amangiri is operated by Aman, the world’s
                                most exclusive ultra-luxury hotel brand.</p>
                        </div>
                    </div>
                    <div class="posts">
                        <div class="col-md-6 col-sm-6 col-xs-12"><img src="./blog_files/15f67c5921e18bb2b370f3634455b2ca.jpg" alt=""></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3> SONEVA KIRI, KOH KOOD, THAILAND</h3>
                            <p>Welcome to my absolute favorite hotel in the world! Remote yet accessible by a 1,5 hour
                                flight from Bangkok on the resort’s private plane, Soneva Kiri sits on Thailand’s fourth
                                largest but least populated island, Koh Kood, in the Gulf of Siam. It has 36 spectacular
                                private villas – amongst the largest in the world – that are built on hillsides
                                blanketed with ancient rainforest and overlooking the turquoise waters. Guest are given
                                their own buggy to get to the private and stunning, coconut palm shaded beach, where
                                your toes will go into the powdery soft sand, and where you can stroll through the
                                pristine waters with the ocean lapping at your calves. Be prepared for a phenomenal
                                experience!</p>
                        </div>
                    </div>
                </div>
                <!--===== POST END ======-->
            </div>
        </div>
    </section>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>