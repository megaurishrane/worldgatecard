<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!-- Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Navigation -->

    <!-- END HEADER SECTION -->

    <!--====== Term and Conditions ==========-->

    <!--====== BANNER ==========-->
    <section>
        <div class="rows inner_banner inner_banner_1" style="padding-top: 210px;">
            <div class="container">
                <h2>Privacy Policy</h2>
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                    </li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="#" class="bread-acti">Privacy Policy</a></li>
                </ul>
                <p>Join and enjoy fabulous room rates and many more facilities with us.</p>
            </div>
        </div>
    </section>
    <!--====== ALL POST ==========-->
    <section>
        <div class="rows inn-page-bg com-colo">
            <div class="container inn-page-con-bg tb-space pad-bot-redu-5" id="inner-page-title">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2>Privacy Policy</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                </div>
                <!--===== POSTS ======-->
                <div class="rows">
                    <div class="posts">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <!-- <h3>Worldwide Reservations Offices </h3> -->
                            <h5 class="desc-p">
                                This Privacy Policy sets out the policy of World Gate with respect to the way we obtain, use, and disclose information about you through website
                            </h5>
                            <h5 class="desc-p">
                                We understand and appreciate that you are concerned about privacy, particularly in relation to the use and disclosure of personal information.
                                We are committed to providing a high level of privacy in relation to all personal information that is collected by us. The purpose of this Privacy Policy
                                is to tell you what kind of information we may gather about you when you visit our site, how we may use that information, whether we disclose it to anyone,
                                and the choices you have regarding how we will use your personal information, and your ability to correct this information. Our website allows you to choose
                                what kind and how much information you provide to us and to control how we use whatever information you give us. Our goal is to provide you with
                                a satisfying experience while allowing you to control your privacy and to give you the means to voice any questions or concerns you may have.
                            </h5>

                            <h3> YOUR CONSENT </h3>
                            <h5 class="desc-p">You consent to your personal information being used in accordance with the privacy policy by visiting our website,
                                by entering a competition on our website,
                                by purchasing our products on the website and/or by providing us with your personal information on the website. </h5>

                            <h3>COLLECTION OF INFORMATION </h3>
                            <h5 class="desc-p">Information is collected from you primarily to make it easier and more rewarding for you to use our website and services. Depending on the service you are accessing, you could be asked to provide information such as your name, email address or information about what you like and do not like. It is entirely your choice whether to respond to these questions or not. </h5>

                            <h3>USE OF PERSONAL INFORMATION </h3>
                            <h5 class="desc-p">World Gate will use the personal information you have chosen to provide us for the purpose for which you provided it. World Gate will not use it for any other purpose without your consent. We might on occasions, use this information to notify you of any important changes to our site or any special promotions that may be of interest to you. On each email or communication that we send you, we will include simple instructions on how you can immediately unsubscribe from our mailing list. You can opt out from receiving such material at any time by emailing our privacy manager and asking to be removed from the mailing list. Any information we may collect from you will be treated with strict confidentiality. </h5>

                            <h3>DISCLOSURE </h3>
                            <h5 class="desc-p">There will be occasions where it will be necessary for World Gate to disclose your personal information to third parties. World Gate may be required to disclose your personal information to third parties to provide the products or services you have requested, for example, if you purchase products online, World Gate will need to disclose your personal information to third parties to bill and deliver your products. However, the disclosure will only be made where it is necessary to fulfill the purpose for which you disclosed your personal information. Otherwise, then stated above, we do not disclose personal information that you may give us, such as your name, address, email address or telephone number, to any organization or person outside BOUNCE, unless you have authorized us to do so, or unless we are requested to do so by a law enforcement agency in relation to a suspected breach of any law. </h5>

                            <h3>NO SALE OF PERSONAL INFORMATION </h3>
                            <h5 class="desc-p">Under no circumstances will World Gate sell or receive payment for licensing or disclosing your personal information. </h5>

                            <h3>PASSWORDS </h3>
                            <h5 class="desc-p">Ultimately, you are solely responsible for maintaining the secrecy of your passwords and/or any personal information. Be responsible whenever you are online. <br>
                                Credit Cards We do not collect or store credit card information on our database or server. This information is managed through a secure credit card payment facility, all information sent to the processing bank is managed with the latest security software, systems, and processes (SSL). </h5>

                            <h3>SECURITY </h3>
                            <h5 class="desc-p">
                                World Gate operates secure data networks that are designed to protect your privacy and security. World Gate has a Security Policy that explains how we protect your privacy and personal information.

                                Please note that our website does not provide systems for secure transmission of personal information across the internet, except where otherwise specifically stated. You should be aware that there are inherent risks in transmitting personal information via the internet and that we accept no responsibility for personal information provided via unsecured websites.

                                We will not be held liable for loss or damage arising from unauthorized access to your personal information or a breach of this policy whether such access or breach was caused by our negligent or willful act or omission or that of our employees or agents.
                            </h5>

                            <h3>ACCESS AND CORRECTION </h3>
                            <h5 class="desc-p">
                                The security of your information is important to us. The information we have collected about you cannot be seen or modified by anyone else. We make all reasonable efforts to ensure that information is stored securely both in electronic and physical form.

                                You may access your information at any time. If you would like to know what information we hold about you, please contact us. If you discover that there is an error or information is missing, please notify us and we will try to correct or update the information as soon as possible.

                                We will try to ensure that all information we collect, use, or disclose about you is accurate, complete and up to date.
                            </h5>

                            <h3> </h3>
                            <h5 class="desc-p"></h5>

                            <h3> </h3>
                            <h5 class="desc-p"></h5>

                        </div>
                    </div>
                    <!--===== POST END ======-->
                </div>
                <div class="rows" style="text-align: center;">
                    <img src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all/privacy-arbic-1.PNG'; ?>" alt="">
                    <img src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all/privacy-arbic-2.PNG'; ?>" alt="">
                    <img src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all/privacy-arbic-3.PNG'; ?>" alt="">
                </div>
            </div>
        </div>
    </section>
    <style>
        .desc-p {

            font-style: normal !important;
            font-weight: 500 !important;
            margin-bottom: 20px !important;
        }
    </style>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/travelTip'); ?>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>