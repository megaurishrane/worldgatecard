<section>
    <div class="rows">
        <div class="footer">
            <div class="container" style="padding-top: 35px;">
                <div class="foot-sec2">
                    <div>
                        <div class="row">
                            <!--								<div class="col-sm-3 foot-spec foot-com">-->
                            <!--									<h4><span>Holiday</span> Tour & Travels</h4>-->
                            <!--									<p>World's leading tour and travels Booking website,Over 30,000 packages worldwide.</p>-->
                            <!--								</div>-->
                            <div class="col-sm-3 foot-spec foot-com">
                                <h4><img src="<?php echo \Fuel\Core\Uri::base(false) . 'assets/all/bahrain.png'; ?>" width="10%" alt="🇸🇦">
                                    <span>Bahrain Contacts</span>
                                </h4>
                                <p>Office 1640, Bldg 1565, <br>Road No. 1722, Block 317,<br>
                                    Diplomat Commercial Office Tower,<br>
                                    Diplomatic Area, Manama, <br>Kingdom of Bahrain</p>
                                <p style="color: #ad974f;"><i class="fa fa-envelope" style="color: #ad974f;"></i>&nbsp;info@worldgatecard.com
                                </p>
                                <!-- <p><i class="fa fa-phone" aria-hidden="true" style="color: #ad974f;"></i> &nbsp;
                                    <span class="highlighted" style="color: #ad974f;">+973 1750 0017</span>
                                </p>
                                <p><i class="fa fa-phone" aria-hidden="true" style="color: #ad974f;"></i> &nbsp;
                                    <span class="highlighted" style="color: #ad974f;">+973 3808 8506</span>
                                </p> -->
                                <p><i class="fa fa-phone" aria-hidden="true" style="color: #ad974f;"></i>
                                    &nbsp;<span class="highlighted" style="color: #ad974f;">+973 3414 0420</span>
                                </p>
                            </div>
                            <div class="col-sm-3 foot-spec foot-com">
                                <h4><img src="<?php echo \Fuel\Core\Uri::base(false) . 'assets/all/egypt.png'; ?>" width="10%" alt="🇸🇦">
                                    <span>Egypt Contacts
                                </h4>
                                <p> 25 B Khufu Entrance, <br>
                                    Hadaiek Al Ahram, Jizah<br>
                                </p>
                                <p style="color: #ad974f;"><i class="fa fa-envelope" style="color: #ad974f;"></i>&nbsp;info@worldgatecard.com
                                </p>
                                <p><i class="fa fa-phone" aria-hidden="true" style="color: #ad974f;"></i> &nbsp;
                                    <span class="highlighted" style="color: #ad974f;">+20233769902</span>
                                </p>
                                <p><i class="fa fa-phone" aria-hidden="true" style="color: #ad974f;"></i> &nbsp;
                                    <span class="highlighted" style="color: #ad974f;">+201555254426</span>
                                </p>
                            </div>
                            <div class="col-sm-3 foot-spec foot-com">
                                <h4><img src="<?php echo \Fuel\Core\Uri::base(false) . 'assets/all/saudhi.png'; ?>" width="10%" alt="🇸🇦">
                                    <span>Saudi Arabia Contacts
                                </h4>
                                <p> Jeddah & Riyadh. <br>
                                </p>
                                <p style="color: #ad974f;"><i class="fa fa-envelope" style="color: #ad974f;"></i>&nbsp;info@worldgatecard.com
                                </p>
                                <p><i class="fa fa-phone" aria-hidden="true" style="color: #ad974f;"></i> &nbsp;
                                    <span class="highlighted" style="color: #ad974f;">+966543664212</span>
                                </p>
                            </div>
                            <div class="col-sm-3 col-md-3 foot-spec foot-com">
                                <h4><span>SUPPORT</span> &amp; HELP</h4>
                                <ul class="two-columns">
                                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>about_us">About Us</a></li>
                                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>register">Be Member </a></li>
                                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>hotel_list">Hotels</a></li>
                                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>term_n_conditions">Terms&amp;Conditions </a></li>
                                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>flight_list">Flights</a></li>
                                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>privacy_policy">Privacy Policy</a></li>
                                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>car_rent">Car</a></li>
                                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>contact_us">Contact Us</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-3 foot-social foot-spec foot-com">

                                <h4><span>Follow</span> with us</h4>
                                <p>Join us on social media platform as stay connected with latest offers and news
                                    related to travel and tour.</p>
                                <ul>
                                    <li><a href="https://www.facebook.com/pg/worldgatecardbahrain"><i class="fa fa-facebook" aria-hidden="true" style="color: #ad974f;"></i></a></li>
                                    <li><a href="https://www.instagram.com/worldgatecard/"><i class="fa fa-instagram" aria-hidden="true" style="color: #ad974f;"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>