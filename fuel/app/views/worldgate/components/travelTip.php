<section>
    <div class="rows tips tips-home tb-space home_title">
        <div class="container tips_1">
            <!-- TIPS BEFORE TRAVEL -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <h3>Tips Before Travel</h3>
                <div class="tips_left tips_left_1">
                    <h5>Bring copies of your passport</h5>
                    <p>It is good idea to keep a copy of your passport will traveling outside your own country.</p>
                </div>
                <div class="tips_left tips_left_2">
                    <h5>Register with your embassy</h5>
                    <p>Let your embassy know your are traveling in country so in any emergency will find your full.
                    </p>
                </div>
                <div class="tips_left tips_left_3">
                    <h5>Always have local cash</h5>
                    <p>Get your currency exchange before traveling at correct rate help will buying things. </p>
                </div>
            </div>
            <!-- CUSTOMER TESTIMONIALS -->
            <div class="col-md-8 col-sm-6 col-xs-12 testi-2">
                <!-- TESTIMONIAL TITLE -->
                <h3>Customer Testimonials</h3>
                <div class="testi">
                    <h4>Gaurish Naresh Rane</h4>
                    <p>Amazing experience with World Gate got a really good deal on hotel and flight travel. </p>
                    <address>Manama, Bahrain</address>
                </div>
                <!-- ARRANGEMENTS & HELPS -->
                <!--                <h3>Arrangement & Helps</h3>-->
                <!--                <div class="arrange">-->
                <!--                    <ul>-->
                <!--                    -->
                <!--                        <li>-->
                <!--                            <a href="#"><img src="-->
                <!--images/Location-Manager.png"-->
                <!--                                             alt=""> </a>-->
                <!--                        </li>-->
                <!--                    -->
                <!--                        <li>-->
                <!--                            <a href="#"><img src="-->
                <!--images/Private-Guide.png"-->
                <!--                                             alt=""> </a>-->
                <!--                        </li>-->
                <!--                      -->
                <!--                        <li>-->
                <!--                            <a href="#"><img src="-->
                <!--images/Arrangements.png"-->
                <!--                                             alt=""> </a>-->
                <!--                        </li>-->
                <!--                     -->
                <!--                        <li>-->
                <!--                            <a href="#"><img-->
                <!--                                        src="-->
                <!--images/Events-Activities.png"-->
                <!--                                        alt=""> </a>-->
                <!--                        </li>-->
                <!--                    </ul>-->
                <!--                </div>-->
            </div>
        </div>
    </div>
</section>