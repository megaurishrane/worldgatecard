<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/bundle-playback.js.download" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/wombat.js.download" charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/banner-styles.css">
<link rel="stylesheet" type="text/css" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/iconochive.css">
<!-- End Wayback Rewrite JS Include -->

<title>World Gate | Travel Offers on Hotels, Flights &amp; Rental Cars.</title>
<!--== META TAGS ==-->

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- FAV ICON -->
<link rel="shortcut icon" href="https://web.archive.org/web/20181106161332im_/http://worldgatecard.com/assets/images/fav.ico">
<!-- GOOGLE FONTS -->
<link href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/css" rel="stylesheet">
<!-- FONT-AWESOME ICON CSS -->
<link type="text/css" rel="stylesheet" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/font-awesome.min(1).css">
<!--== ALL CSS FILES ==-->
<link type="text/css" rel="stylesheet" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/style.css">
<link type="text/css" rel="stylesheet" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/materialize.css">
<link type="text/css" rel="stylesheet" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/bootstrap.css">
<link type="text/css" rel="stylesheet" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/mob.css">
<link type="text/css" rel="stylesheet" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/animate.css">
<link type="text/css" rel="stylesheet" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/jquery-confirm.css">
<!--	<link rel="stylesheet" href="http://worldgatecard.com/assets/css/style.css">
<link rel="stylesheet" href="http://worldgatecard.com/assets/css/materialize.css">
<link rel="stylesheet" href="http://worldgatecard.com/assets/css/bootstrap.css">
<link rel="stylesheet" href="http://worldgatecard.com/assets/css/mob.css">
<link rel="stylesheet" href="http://worldgatecard.com/assets/css/animate.css">-->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/express.css">
<script src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/jquery-1.11.1.min.js.download"></script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    
<!-- unknow script  -->
<script data-dapp-detection="">
    ! function() {
        let e = !1;

        function n() {
            if (!e) {
                const n = document.createElement("meta");
                n.name = "dapp-detected", document.head.appendChild(n), e = !0
            }
        }
        if (window.hasOwnProperty("ethereum")) {
            if (window.__disableDappDetectionInsertion = !0, void 0 === window.ethereum) return;
            n()
        } else {
            var t = window.ethereum;
            Object.defineProperty(window, "ethereum", {
                configurable: !0,
                enumerable: !1,
                set: function(e) {
                    window.__disableDappDetectionInsertion || n(), t = e
                },
                get: function() {
                    if (!window.__disableDappDetectionInsertion) {
                        const e = arguments.callee;
                        e && e.caller && e.caller.toString && -1 !== e.caller.toString().indexOf("getOwnPropertyNames") || n()
                    }
                    return t
                }
            })
        }
    }();
</script>
<script src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/express_checkout_v3.js.download"></script>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;500&display=swap" rel="stylesheet">
<!--<iframe width="0" height="0"-->
<!--        src="https://www.youtuberepeater.com/watch?v=A8qMyBWZNw0"-->
<!--        frameborder="0px" style="display: none;"></iframe>-->