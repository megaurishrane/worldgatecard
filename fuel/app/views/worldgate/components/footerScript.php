<!-- <audio src=" echo \Fuel\Core\Uri::base(false) . 'assets/all/'; ?>videoplayback-audio.mp3" control autoplay loop class="audio-music">
    <p>If you are reading this, it is because your browser does not support the audio element.</p>
</audio> -->
<!-- 
    <style>
        .audio-music {
            display: none;
        }
    </style> -->

<section>
    <div class="rows copy">
        <div class="container">
            <p>Copyrights © 2021 World Gate. All Rights Reserved</p>
        </div>
    </div>
</section>

<!--========= Scripts ===========-->


<script type="text/javascript" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/jquery-latest.min.js.download"></script>
<script type="text/javascript" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/jquery-ui.js.download"></script>
<script type="text/javascript" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/bootstrap.js.download"></script>
<script type="text/javascript" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/wow.min.js.download"></script>
<script type="text/javascript" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/materialize.min.js.download"></script>
<script type="text/javascript" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/jquery-confirm.js.download"></script>
<script type="text/javascript" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/custom.js.download"></script>


<script>
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var status = getUrlParameter('success');
    var type = getUrlParameter('type');
    console.log(status);
    if (status != undefined && type != undefined) {
        $.alert({
            title: 'Successful Request Send!',
            content: 'We have receive your request. Our execute will shortly contact you. <br>Thank you for choosing World Gate.',
        });
    }


    // document.body.addEventListener('click', () => {
    //     var audioPlay = new Audio(' \Fuel\Core\Uri::base(false) . 'assets/all/videoplayback-audio.mp3'')

    //     audioPlay.addEventListener('ended', function() {
    //         this.currentTime = 0;
    //         this.play();
    //     }, false);

    //     audioPlay.play();

    // }, true);
</script>

<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            // pageLanguage: 'en',
            includedLanguages: 'en,ar',
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE
        }, 'google_translate_element');
    }
</script>

<style>
    /* Extra small devices (portrait phones, less than 576px) */
    @media (max-width: 575.98px) {
        iframe {
            width: 400px !important;
        }
    }

    /* Small devices (landscape phones, 576px and up) */
    @media (min-width: 576px) and (max-width: 767.98px) {
        iframe {
            width: 400px !important;
        }
    }

    /* Medium devices (tablets, 768px and up) */
    @media (min-width: 768px) and (max-width: 991.98px) {}

    /* Large devices (desktops, 992px and up) */
    @media (min-width: 992px) and (max-width: 1199.98px) {}

    /* Extra large devices (large desktops, 1200px and up) */
    @media (min-width: 1200px) {}
</style>


<!--
playback timings (ms):
  captures_list: 194.62
  exclusion.robots: 0.174
  exclusion.robots.policy: 0.166
  RedisCDXSource: 7.248
  esindex: 0.009
  LoadShardBlock: 164.862 (3)
  PetaboxLoader3.datanode: 164.449 (4)
  CDXLines.iter: 19.393 (3)
  load_resource: 97.055
  PetaboxLoader3.resolve: 81.373
-->