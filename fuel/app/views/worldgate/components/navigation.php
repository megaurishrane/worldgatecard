<!--====== MOBILE MENU ==========-->

<section class="mob-top">
    <div class="mob-menu">
        <div class="mob-head-left">
            <img src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/world_gate_logo.jpg" alt="" width="45%">
        </div>
        <div class="mob-head-right"><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>#"><i class="fa fa-bars mob-menu-icon" aria-hidden="true"></i></a> <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a>
            <div class="mob-menu-slide">
                <br>
                <h4 style="color: black;">Menu</h4>
                <ul class="top-menu">
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>index">
                            <i class="fa fa-home" aria-hidden="true"></i> Home</a>
                    </li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>member_benefits">
                            <i class="fa fa-user" aria-hidden="true"></i>Member Benefits</a>
                    </li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>hotelList">
                            <i class="fa fa-building" aria-hidden="true"></i>Hotels</a>
                    </li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>flight_list">
                            <i class="fa fa-plane" aria-hidden="true"></i>Flights</a>
                    </li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>car_rent">
                            <i class="fa fa-car" aria-hidden="true"></i>Car Rents</a>
                    </li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>about_us">
                            <i class="fa fa-users" aria-hidden="true"></i>About us</a>
                    </li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>blogs">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>Blog</a>
                    </li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>contact_us">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            Contact Us</a>
                    </li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>login">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                            <strong>Login</strong></a>
                    </li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>paytab">
                            <i class="fa fa-credit-card" aria-hidden="true"></i>
                            <strong>Make Payment</strong></a>
                    </li>
                </ul>


            </div>
        </div>
    </div>
</section>
<!--====== END MOBILE MENU ==========-->
<!--====== TOP HEADER ==========-->

<section>
    <div class="rows head" data-offset-top="120">
        <div class="container">
            <div>
                <!--====== BRANDING LOGO ==========-->
                <div class="col-md-4 col-sm-12 col-xs-12 head_left">
                    <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>index">
                        <img src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/world_gate_logo.jpg" alt="" width="45%"> </a>
                </div>
                <!--====== HELP LINE & EMAIL ID ==========-->
                <div class="col-md-8 col-sm-12 col-xs-12 head_right head_right_all" style="margin-top: 30px;">
                    <ul>
                        <li>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>#">Bahrain Support
                                Line: +973 3414 0420 </a><br>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>#">Egypt Support
                                Line: +20155525 4426</a><br>
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>#">Saudi Arabia Support
                                Line: +96654366 4212</a><br>

                        </li>
                        <li style="vertical-align: top;">
                            <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>#">Email: info@worldgatecard.com</a>
                            <div id="google_translate_element"></div>
                        </li>

                        <li style="vertical-align: top;">

                            <a class="dropdown-button waves-effect waves-light profile-btn" href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>" data-activates="myacc">
                                <i class="fa fa-user" aria-hidden="true"></i> Login
                            </a>

                            <ul id="myacc" class="dropdown-content v2_head_right">
                                <li>
                                    <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>login" style="color: black !important;"><i class="fa fa-user" aria-hidden="true"></i>Login</a>
                                </li>
                                <li>
                                    <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>paytab" style="color: black !important;"><i class="fa fa-payment" aria-hidden="true"></i>Make Payment</a>
                                </li>
                            </ul>

                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--====== NAVIGATION MENU ==========-->
<section>
    <div class="main-menu-v2">
        <div class="container">
            <div class="row navigation">
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>index">Home</a>
                    </li>

                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>member_benefits">Member
                            Benefits</a></li>
                    <li>
                        <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>hotel_list" class="dropbtn">Service's</a>
                        <ul class="dropdown">
                            <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>hotel_list">Hotels</a>
                            </li>
                            <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>flight_list">Flights</a>
                            </li>
                            <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>car_rent">Car
                                    Rentals</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>about_us">About
                            us</a></li>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>blogs">Blogs</a>
                    </li>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>contact_us">Contact
                            Us</a></li>
                    <li id="main-menu-v2-book"><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/' ?>register">Be
                            Member</a>
                    </li>
                </ul>
                <div>
                    <div>
                        <div>
                            <div class="all-drop-menu">
                                <!-- PACKAGE DROP DOWN MENU -->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--navigation style-->
<style>
    .navigation ul li {
        display: inline-block;
        position: relative;
        line-height: 21px;
        text-align: left;
    }

    .navigation ul li a {
        display: block;
        padding: 0px 10px;
        color: #EAEAEA;
        text-decoration: none;
    }

    .navigation ul li a:hover {
        color: gray;
    }

    .navigation ul li ul.dropdown {
        min-width: 160%;
        /* Set width of the dropdown */
        background: #231F20;
        display: none;
        position: absolute;
        z-index: 999;
        left: 0;
    }

    .navigation ul li:hover ul.dropdown {
        display: block;
        /* Display the dropdown */
    }

    .navigation ul li ul.dropdown li {
        display: block;
    }
</style>