<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!--Header & Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Header & Navigation -->

    <!--END HEADER SECTION-->
    <!--====== HOTELS LIST ==========-->
    <section class="hot-page2-alp hot-page2-pa-sp-top">
        <div class="container">
            <div class="row inner_banner inner_banner_3 bg-none">
                <div class="hot-page2-alp-tit">
                    <h1>Hotel &amp; Restaurants</h1>
                    <ul>
                        <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                        </li>
                        <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                        <li><a href="#" class="bread-acti">Hotels &amp; Restaurants</a></li>
                    </ul>
                    <p>World's leading Hotel Booking website,Over 40,000 Hotel rooms worldwide. </p>
                </div>
            </div>
            <div class="row">
                <div class="hot-page2-alp-con">
                    <!--LEFT LISTINGS-->
                    <div class="col-md-3 hot-page2-alp-con-left">
                        <!--PART 1 : LEFT LISTINGS-->
                        <div class="hot-page2-alp-con-left-1">
                            <h3>Hotels Offers</h3>
                        </div>
                        <!--PART 2 : LEFT LISTINGS-->
                        <div class="hot-page2-hom-pre hot-page2-alp-left-ner-notb">
                            <ins class="bookingaff" data-aid="1465186" data-target_aid="1447482" data-prod="banner" data-width="250" data-height="250" data-lang="en-US">
                                <!-- Anything inside will go away once widget is loaded. -->
                                <a href="https://web.archive.org/web/20181015130653/http://www.booking.com/?aid=1447482">Booking.com</a>
                            </ins>
                            <script type="text/javascript">
                                (function(d, sc, u) {
                                    var s = d.createElement(sc),
                                        p = d.getElementsByTagName(sc)[0];
                                    s.type = 'text/javascript';
                                    s.async = true;
                                    s.src = u + '?v=' + (+new Date());
                                    p.parentNode.insertBefore(s, p);
                                })(document, 'script', '//web.archive.org/web/20181015130653/http://aff.bstatic.com/static/affiliate_base/js/flexiproduct.js');
                            </script>
                        </div>
                        <!--PART 7 : LEFT LISTINGS-->
                        <div class="hot-page2-hom-pre hot-page2-alp-left-ner-notb">
                            <ins class="bookingaff" data-aid="1465187" data-target_aid="1447482" data-prod="banner" data-width="250" data-height="250" data-lang="en-US">
                                <!-- Anything inside will go away once widget is loaded. -->
                                <a href="https://web.archive.org/web/20181015130653/http://www.booking.com/?aid=1447482">Booking.com</a>
                            </ins>
                            <script type="text/javascript">
                                (function(d, sc, u) {
                                    var s = d.createElement(sc),
                                        p = d.getElementsByTagName(sc)[0];
                                    s.type = 'text/javascript';
                                    s.async = true;
                                    s.src = u + '?v=' + (+new Date());
                                    p.parentNode.insertBefore(s, p);
                                })(document, 'script', '//web.archive.org/web/20181015130653/http://aff.bstatic.com/static/affiliate_base/js/flexiproduct.js');
                            </script>
                        </div>
                        <!--PART 4 : LEFT LISTINGS-->


                    </div>
                    <!--END LEFT LISTINGS-->
                    <!--RIGHT LISTINGS-->
                    <div class="col-md-9 hot-page2-alp-con-right">
                        <div class="hot-page2-alp-con-right-1">
                            <!--LISTINGS-->
                            <div class="row">
                                <ins class="bookingaff" data-aid="1447635" data-target_aid="1447635" data-prod="nsb" data-width="100%" data-height="auto" data-lang="xu" data-currency="BHD" data-df_num_properties="3">
                                    <!-- Anything inside will go away once widget is loaded. -->
                                    <a href="https://web.archive.org/web/20181015130653/http://www.booking.com/?aid=1447635">Booking.com</a>
                                </ins>
                                <script type="text/javascript">
                                    (function(d, sc, u) {
                                        var s = d.createElement(sc),
                                            p = d.getElementsByTagName(sc)[0];
                                        s.type = 'text/javascript';
                                        s.async = true;
                                        s.src = u + '?v=' + (+new Date());
                                        p.parentNode.insertBefore(s, p);
                                    })(document, 'script', '//web.archive.org/web/20181015130653/http://aff.bstatic.com/static/affiliate_base/js/flexiproduct.js');
                                </script>


                                <ins class="bookingaff" data-aid="1466178" data-target_aid="1447482" data-prod="banner" data-width="728" data-height="90" data-lang="en-US">
                                    <!-- Anything inside will go away once widget is loaded. -->
                                    <a href="https://web.archive.org/web/20181015130653/http://www.booking.com/?aid=1447482">Booking.com</a>
                                </ins>
                                <script type="text/javascript">
                                    (function(d, sc, u) {
                                        var s = d.createElement(sc),
                                            p = d.getElementsByTagName(sc)[0];
                                        s.type = 'text/javascript';
                                        s.async = true;
                                        s.src = u + '?v=' + (+new Date());
                                        p.parentNode.insertBefore(s, p);
                                    })(document, 'script', '//web.archive.org/web/20181015130653/http://aff.bstatic.com/static/affiliate_base/js/flexiproduct.js');
                                </script>
                            </div>
                        </div>
                    </div>
                    <!--END RIGHT LISTINGS-->
                </div>
            </div>
        </div>
    </section>

    <style>
        .select-dropdown {
            background-color: #cecece !important;
        }
    </style>
    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <section>
        <div class="rows tips tips-home tb-space home_title">
            <div class="container tips_1">
                <!-- TIPS BEFORE TRAVEL -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h3>Tips Before Travel</h3>
                    <div class="tips_left tips_left_1">
                        <h5>Bring copies of your passport</h5>
                        <p>It is good idea to keep a copy of your passport will traveling outside your own country.</p>
                    </div>
                    <div class="tips_left tips_left_2">
                        <h5>Register with your embassy</h5>
                        <p>Let your embassy know your are traveling in country so in any emergency will find your full.
                        </p>
                    </div>
                    <div class="tips_left tips_left_3">
                        <h5>Always have local cash</h5>
                        <p>Get your currency exchange before traveling at correct rate help will buying things. </p>
                    </div>
                </div>
                <!-- CUSTOMER TESTIMONIALS -->
                <div class="col-md-8 col-sm-6 col-xs-12 testi-2">
                    <!-- TESTIMONIAL TITLE -->
                    <h3>Customer Testimonials</h3>
                    <div class="testi">
                        <h4>Gaurish Naresh Rane</h4>
                        <p>Amazing experience with World Gate got a really good deal on hotel and flight travel. </p>
                        <address>Manama, Bahrain</address>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>