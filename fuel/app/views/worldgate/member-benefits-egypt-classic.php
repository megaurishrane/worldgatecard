<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!-- Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Navigation -->

    <!-- END HEADER SECTION -->

    <!--====== Benfits with World gate member ==========-->
    <section>
        <div class="rows inner_banner inner_banner_1" style="padding-top: 210px;">
            <div class="container">
                <h2>Black Member Benefits</h2>
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                    </li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits'; ?>">Egypt Member Benefits</a></li>
                    <li><a href="#" class="bread-acti">Classic Membership</a></li>
                </ul>
                <p>Join and enjoy fabulous room rates and many more facilities with us.</p>
            </div>
        </div>
    </section>
    <!--====== ALL POST ==========-->
    <section>
        <div class="rows inn-page-bg com-colo">
            <div class="container inn-page-con-bg tb-space pad-bot-redu-5" id="inner-page-title">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2>World Gate <span>Classic</span> Program</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>عميلنا العزيز نود ان نشكرك علي ثقتتك و انضمامك الينا لتكون أحد عملاء شركتنا حيث تتمتع بالعديد من المزايا و العروض في برنامج الكلاسيك صالحه لمده عام.</p>
                    <h4 style="color:#AD974F;">***** نعدك بان نكون خيارك الاول في سفرك بأمان دائما *****</h4>
                </div>
                <!--===== POSTS ======-->
                <div class="rows">
                    <div class="posts">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h4 style="color:#AD974F;">مزايا برنامج الكلاسيك:-</h4>

                            <p style="font-size: larger;">- تخفيض 10% علي الفنادق الاربعة او الخمسة نجوم من خلال موقع بوكينج.</p>
                            <p style="font-size: larger;">- افضل سعر لحجز تذاكر الطيران علي جميع الخطوط لجميع انحاء العالم.</p>
                            <p style="font-size: larger;">- تخفيض 10% علي ايجار السيارات لجميع دول العالم.</p>

                            <h4 style="color:#AD974F;">قسائم برنامج الكلاسيك:-</h4>
                            <p style="font-size: larger;"> - عدد 1 كوبون ليلة مجانية لشخصين (بحد اقصي 1200 جنية).</p>
                            <p style="font-size: larger;"> - عدد 1 كوبون خصم 1000 جنيه عند حجز اقامه فندقية لمدة 6 ليالي متتالية في واحدا من (القاهرة، الاسكندرية، شرم الشيخ، الغردقة، الاقصر، اسوان).</p>
                            <p style="font-size: larger;"> - عدد 1 كوبون اقامة 4 ليالي بسعر 3 ليالي في فنادق 4 او 5 نجوم.</p>
                            <p style="font-size: larger;"> - عدد 1 كوبون اقامة 3 ليالي بسعر ليلتين في فنادق 4 او 5 نجوم.</p>
                            <p style="font-size: larger;"> - عدد 1 كوبون خصم 50% علي تذكرة الطيران عند حجز 7 ليالي متتالية في اوروبا في فنادق 4 نجوم او كوبون تذكرة طيران مجانية ذهابًا وإيابًا في اوروبا عند حجز 7 ليالي متتالية في فنادق 5 نجوم. </p>
                            <p style="font-size: larger;"> - عدد 1 كوبون تذكرة طيران مجانية ذهابًا وإيابًا إلى دبي أو اسطنبول عند حجز إقامة لمدة 7 ليالٍ متتالية في فنادق 4 او 5 نجوم.</p>
                            <p style="font-size: larger;"> - عدد 1 كوبون توصيل مجاني في مصر ذهاب او عودة لشخصين.</p>
                            <p style="font-size: larger;"> - عدد 1 كوبون دعوه غداء او عشاء مجانية لشخصين داخل القاهرة.</p>



                            <h4 style="color:#AD974F;"> الاحكام و الشروط:-</h4>
                            <p style="font-size: larger;">- العضوية شخصية و لا يجوز التنازل عنها لشخص اخر.</p>
                            <p style="font-size: larger;">- الكوبون المراد استخدامه يكون بتوقيع العميل عليه و ارساله لخدمه العملاء و استخدام الكوبون يكون من خلال مكاتبنا.</p>
                            <p style="font-size: larger;">- لا يجوز الجمع بين اي كوبون او عرض تخفيضي اخر.</p>

                            <p style="font-size: larger;">• BY SIGNING THE AGREEMENT CONSTITUTES ACCEPTANCE OF THE T&C WWW.WORLDGATECARD.COM</p>
                            <p style="font-size: larger;">• REQUEST MUST BE MADE IN ADVANCE BEFORE THE TIME OF RESERVATION.</p>
                            <p style="font-size: larger;">• CERTIFICATES CANNOT BE USE WITH BENEFITS AT SAME TIME.</p>
                            <p style="font-size: larger;">• USING CERTIFICATES ON BOOKING.COM BASIC PRICE.</p>


                            <h4 style="color:#AD974F;"> نحيط سيادتكم علما بان الموقع الخاص بنا هو موقع خاص للعملاء فقط و قد راعينا فيه المواصفات و الخدمات العالمية و فروق الاسعار في الخدمات املين ان تنال رضاكم دائما.</h4>


                            <!-- <h3 style="color:#AD974F;">Basic Lifetime Features of the "BLACK" Program</h3> -->
                            <!-- <p style="font-size: larger;">• 2 Free dinner coupons for two people on one of the Nile boats in Cairo.</p> -->

                            <a href="https://web.archive.org/web/20181114145021/http://worldgatecard.com/register" class="link-btn">Register Now</a>
                        </div>
                    </div>
                </div>
                <!--===== POST END ======-->
            </div>
        </div>
    </section>

    <style>
        .desc-p {

            font-style: normal !important;
            font-weight: 500 !important;
            margin-bottom: 20px !important;
        }
    </style>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <section>
        <div class="rows tips tips-home tb-space home_title">
            <div class="container tips_1">
                <!-- TIPS BEFORE TRAVEL -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h3>Tips Before Travel</h3>
                    <div class="tips_left tips_left_1">
                        <h5>Bring copies of your passport</h5>
                        <p>It is good idea to keep a copy of your passport will traveling outside your own country.</p>
                    </div>
                    <div class="tips_left tips_left_2">
                        <h5>Register with your embassy</h5>
                        <p>Let your embassy know your are traveling in country so in any emergency will find your full.
                        </p>
                    </div>
                    <div class="tips_left tips_left_3">
                        <h5>Always have local cash</h5>
                        <p>Get your currency exchange before traveling at correct rate help will buying things. </p>
                    </div>
                </div>
                <!-- CUSTOMER TESTIMONIALS -->
                <div class="col-md-8 col-sm-6 col-xs-12 testi-2">
                    <!-- TESTIMONIAL TITLE -->
                    <h3>Customer Testimonials</h3>
                    <div class="testi">
                        <h4>Gaurish Naresh Rane</h4>
                        <p>Amazing experience with World Gate got a really good deal on hotel and flight travel. </p>
                        <address>Manama, Bahrain</address>
                    </div>
                    <!-- ARRANGEMENTS & HELPS -->
                    <!--                <h3>Arrangement & Helps</h3>-->
                    <!--                <div class="arrange">-->
                    <!--                    <ul>-->
                    <!--                    -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="-->
                    <!--images/Location-Manager.png"-->
                    <!--                                             alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                    -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="-->
                    <!--images/Private-Guide.png"-->
                    <!--                                             alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                      -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="-->
                    <!--images/Arrangements.png"-->
                    <!--                                             alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                     -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img-->
                    <!--                                        src="-->
                    <!--images/Events-Activities.png"-->
                    <!--                                        alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                    </ul>-->
                    <!--                </div>-->
                </div>
            </div>
        </div>
    </section>

    <section>

    </section>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>