<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!-- Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Navigation -->

    <!-- END HEADER SECTION -->

    <!--====== Term and Conditions ==========-->

    <!--====== BANNER ==========-->
    <section>
        <div class="rows inner_banner inner_banner_1" style="padding-top: 210px;">
            <div class="container">
                <h2>Terms &amp; Conditions</h2>
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                    </li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="#" class="bread-acti">Terms &amp; Conditions</a></li>
                </ul>
                <p>Join and enjoy fabulous room rates and many more facilities with us.</p>
            </div>
        </div>
    </section>
    <!--====== ALL POST ==========-->
    <section>
        <div class="rows inn-page-bg com-colo">
            <div class="container inn-page-con-bg tb-space pad-bot-redu-5" id="inner-page-title">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2>Term &amp; Conditions</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                </div>
                <!--===== POSTS ======-->
                <div class="rows">
                    <div class="posts">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h3>Worldwide Reservations Offices </h3>
                            <h5 class="desc-p">
                                By calling the telephone number closest to your location, you may speak to any one of our reservation agents. This complimentary service is dedicated to simplifying your needs by making the reservation for you
                            </h5>
                            <h3>Worldwide Member Services </h3>
                            <h5 class="desc-p">• For your convenience, WORLD GATE maintains customer service center. Whether you have a question about your membership benefits or you would like to make a hotel reservation, a WORLD GATE representative will gladly assist you.
                            </h5>
                            <h5 class="desc-p">
                                • Upon joining WORLD GATE, you will receive your personalized WORLD GATE membership card as well as a membership guide outlining your benefits. By signing up for WORLD GATE you will be kept informed of any program development including additional participating hotels, relevant promotional offers and additional benefits made available to members.
                            </h5>
                            <h5 class="desc-p">
                                • To join WORLD GATE simply complete the Contact Us form here and we will personally reach out: https://www.worldgatecard.com.
                            </h5>
                            <h5 class="desc-p">Acceptance: Use of membership card constitutes acceptance of the terms & conditions listed herein. </h5>
                            <h5 class="desc-p">
                                • WORLD GATE membership: We may accept or reject any application for WORLD GATE membership, at our sole discretion. The membership is the property of the issuing agent and must be returned upon request.
                            </h5>
                            <h5 class="desc-p">
                                • The membership is valid for long life. The vouchers for one year from initial purchase date. The membership has no cash value.
                            </h5>
                            <h5 class="desc-p">
                                • Participant hotels and benefits are subject to changes. Members will be updated on the list of participating hotels through the WORLD GATE website, online newsletter and email transmissions sent by WORLD GATE from time to time.
                            </h5>
                            <h5 class="desc-p">
                                • Membership benefit conditions: All membership benefits are valid only during the membership term and shall automatically expire thereafter. All benefits are subject to availability, valid seven days a week, excluding black-out dates, vouchers may not be used in conjunction with any other discount or promotional offer.
                            </h5>
                            <h5 class="desc-p">
                                • Black-Out Dates: All benefits shall not be valid on the following dates: Christmas Eve, Christmas Day, New Year’s Eve, New Year’s Day, Valentine’s Day, Mother’s Day.
                            </h5>
                            <h5 class="desc-p">
                                • Membership certificates: must have notified us of the use of certificates before seven days of reservation. Certificates are valid seven days a week up to the expiration date printed on said certificates. Validity will not be extended beyond the printed expiration date. May not be used in conjunction with any other discount/ promotional offer. Certificates cannot be resold once issued to a member.
                            </h5>

                            <h5 class="desc-p">
                                • Lost /Stolen membership materials: All lost or stolen certificates will not be replaced. Lost or stolen membership cards must be reported to Customer Service. A processing fee of twenty-five dollars (US $50.00) will be charged for each replacement membership card
                            </h5>
                            <h5 class="desc-p">
                                • Cancellations: According to Article No. (14) of the executive regulations of the Consumer Protection Law, the consumer has the right to return the commodity with a refund of its value, within fifteen days from the date of purchasing the commodity, that is if the commodity is affected by defect or if it does not conform to the legally approved specifications or purpose For which it was contracted. but no refund will be issued until the membership card and all certificates have been returned unused to a Customer Service Office. Refund amount will be excluding shipping & handling charges and all expenses within 5 working days.
                            </h5>

                            <h3>Transferable and Non-Transferable Benefits: </h3>
                            <h5 class="desc-p">
                                • A. Transferable: Benefits may be used by the Member or by a Member’s guest. The Member/ Membership Card need not be present at time of guests’ use of the benefit.
                            </h5>
                            <h5 class="desc-p">
                                • B. Non-Transferable: Benefits may be used only by the Member and upon presentation of their valid Membership Card. Proof of identification may be required by HOTEL.
                            </h5>
                            <h3> </h3>
                        </div>
                    </div>
                    <!--===== POST END ======-->
                </div>
            </div>
        </div>
    </section>
    <style>
        .desc-p {

            font-style: normal !important;
            font-weight: 500 !important;
            margin-bottom: 20px !important;
        }
    </style>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/travelTip'); ?>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>