<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!--Header & Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Header & Navigation -->

    <!--End HEADER SECTION-->

    <!--====== BANNER ==========-->
    <section>
        <div class="rows inner_banner inner_banner_1" style="padding-top: 210px;">
            <div class="container">
                <h2>About us</h2>
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                    </li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="#" class="bread-acti">About us</a></li>
                </ul>
                <p>All you need to know about us.</p>
            </div>
        </div>
    </section>
    <!--====== ALL POST ==========-->
    <section>
        <div class="rows inn-page-bg com-colo">
            <div class="container inn-page-con-bg tb-space pad-bot-redu-5" id="inner-page-title">
                <!--===== POSTS ======-->
                <div class="rows">
                    <div class="posts">
                        <div class="col-md-3 col-sm-3 col-xs-12"><img src="<?php echo \Fuel\Core\Uri::base(false) . 'assets/all/' ?>about_profile.jpg" alt="">
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <h3>Mr. Wael Akareb</h3>
                            <h5><span class="post_author">President &amp; Chief Executive Officer</span><span class="post_date">Co-founder</span></h5>
                            <p>Mr. Wael Akareb, co-founder, is responsible for the overall leadership, direction and
                                development of the Company. Prior to founding World Gate.</p>
                            <p>
                                Mr. Wael Akareb work as Sales Manager at Hospitality Dynamics.
                                (Semiramis Intercontinental Cairo, Nile Hilton, Conrad Cairo, Intercontinental City
                                Stars
                                Cairo).
                            </p>
                            <p>
                                Regional Business Development Manager at Hospitality Marketing Concepts HMC
                                (Bahrain-Qatar-UAE-Oman-Jordan-Kuwait-Cairo-KSA).
                            </p>
                            <p>Country Manager at club World(Bahrain).</p>

                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="rows">
                    <div class="posts">
                        <div class="col-md-3 col-sm-3 col-xs-12"><img src="<?php echo \Fuel\Core\Uri::base(false) . 'assets/all/' ?>raba.jpeg" alt=""></div>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <h3>Raghad Al Tikriti</h3>
                            <h5><span class="post_author">Reservation Manager</span></h5>
                            <p>10 years experience in ticketing and tour packaging<br>
                                2009 reservation ticketing in Iraqi airways.<br>
                                2010 reservation ticketing in al Faisal travel.<br>
                                2013 reservation manager Akasa travel.<br>
                                2016 supervisor sanad travel.<br></p>

                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="rows">
                    <div class="posts">
                        <div class="col-md-3 col-sm-3 col-xs-12"><img src="<?php echo \Fuel\Core\Uri::base(false) . 'assets/all/' ?>ahmed.jpeg" alt=""></div>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <h3>Ahmed Mahdi</h3>
                            <p>
                                SPECIAL PERSON COMPANY (SPC) 100 FOREIGN OWNERSHIP<br>
                                ►ALIMITED LIABILITY COMPANY (WLL) 100 FOREIGN OWNERSHIP<br>
                                ► RENEWAL OF CR - REGISTRATION OF COMMERCIAL - MEMBER SHIP CARD- STAMPING<br>
                                ►REGISTRATION OF COMPANY<br>
                                ► ISSUING NEW PERMIT Investor and Businessmen<br>
                                ► ISSUING NEW WORK PERMIT<br>
                                ► RENEWAL OF WORK PERMIT<br>
                                ►UPDATE INFORMATION - CHANGE OF OCCUPATION- CHECKING LEGAL AFFAIR- HOUSEMAID<br>
                                ► BUSINESS VISAS<br>
                                ► ISSUNG-RPENTRYVISA- RP RENEWAL<br>
                                ► ISSUING&amp;TRANSFER WORK PERMIT<br>
                                ► UPDATE INFORMATION-CANCELL RP-RPRPVISA<br>
                                ► ISSUING VISIT VISA FAMILY<br>
                                ► ISSUING OR RENEWAL BAHRAINI PSSPORT<br>
                                ► ISSUING ID CARD<br>
                                ► Renewal of ID card<br>
                                ► Update information<br>
                                ► Address card<br>
                                ► TAKING APPOINMENT<br>
                                ► APROVAL FOR CR OPENING<br>
                                ► UPDATE INFORMATION ADDRESS<br>
                                ► CERTIFICATE<br>
                                ► STAMPING IN FOREING MINISTY<br>
                                ► STAMPING IN FOREING EMBASSIES<br>
                                ► ENVIRNMENTAL AFFAIRS<br>
                                ► CIVIL DEFENCE WORK<br>
                            </p>

                            <p></p>
                        </div>
                    </div>
                </div>
                <!--===== POST END ======-->
            </div>
        </div>
    </section>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <section>
        <div class="rows tips tips-home tb-space home_title">
            <div class="container tips_1">
                <!-- TIPS BEFORE TRAVEL -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h3>Tips Before Travel</h3>
                    <div class="tips_left tips_left_1">
                        <h5>Bring copies of your passport</h5>
                        <p>It is good idea to keep a copy of your passport will traveling outside your own country.</p>
                    </div>
                    <div class="tips_left tips_left_2">
                        <h5>Register with your embassy</h5>
                        <p>Let your embassy know your are traveling in country so in any emergency will find your full.
                        </p>
                    </div>
                    <div class="tips_left tips_left_3">
                        <h5>Always have local cash</h5>
                        <p>Get your currency exchange before traveling at correct rate help will buying things. </p>
                    </div>
                </div>
                <!-- CUSTOMER TESTIMONIALS -->
                <div class="col-md-8 col-sm-6 col-xs-12 testi-2">
                    <!-- TESTIMONIAL TITLE -->
                    <h3>Customer Testimonials</h3>
                    <div class="testi">
                        <h4>Gaurish Naresh Rane</h4>
                        <p>Amazing experience with World Gate got a really good deal on hotel and flight travel. </p>
                        <address>Manama, Bahrain</address>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>