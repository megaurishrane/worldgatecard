<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!--Header & Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Header & Navigation -->

    <!--HEADER SECTION-->
    <section>
        <div class="v2-hom-search" style="background: url(&#39;https://web.archive.org/web/20181011123028im_/http://worldgatecard.com/assets/img/taxi_img_ad.jpg&#39;) no-repeat;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="">
                            <form class="v2-search-form" method="post">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="text" id="select-city" class="autocomplete" name="pick_up_location" style="background-color: #cecece">
                                        <label for="select-city">Pick up location</label>
                                        <ul class="autocomplete-content dropdown-content"></ul>
                                    </div>
                                    <div class="input-field col s12">
                                        <input type="text" id="select-city-1" class="autocomplete" name="drop_location" style="background-color: #cecece">
                                        <label for="select-city">Dropping off location</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input type="text" id="from" name="pick_up_date" style="background-color: #cecece" class="hasDatepicker">
                                        <label for="from">Pick up date</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <div class="select-wrapper"><span class="caret">▼</span>
                                            <!-- <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-1b692338-e56f-10ab-d848-2d42a1c7f76a" value="Pick up time"> -->
                                            <select name="pick_up_time" class="initialized">
                                                <option value="" disabled="" selected="">Pick up time</option>
                                                <option>12:00 AM</option>
                                                <option>01:00 AM</option>
                                                <option>02:00 AM</option>
                                                <option>03:00 AM</option>
                                                <option>04:00 AM</option>
                                                <option>05:00 AM</option>
                                                <option>06:00 AM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input type="text" id="to" name="drop_off_date" style="background-color: #cecece" class="hasDatepicker">
                                        <label for="to">Drop off date</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <div class="select-wrapper"><span class="caret">▼</span>
                                            <!-- <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-580c3968-eb4a-d077-4238-1e92012e26e2" value="Drop off time"> -->
                                            <select name="drop_off_time" class="initialized">
                                                <option value="" disabled="" selected="">Drop off time</option>
                                                <option>12:00 AM</option>
                                                <option>01:00 AM</option>
                                                <option>02:00 AM</option>
                                                <option>03:00 AM</option>
                                                <option>04:00 AM</option>
                                                <option>05:00 AM</option>
                                                <option>06:00 AM</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <div class="select-wrapper"><span class="caret">▼</span>
                                            <!-- <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-fbee54db-5211-ee06-362a-2e6f5f880fc5" value="Select car type"> -->
                                            <select name="car_type" class="initialized">
                                                <option value="" disabled="" selected="">Select car type</option>
                                                <option>Micro</option>
                                                <option>Mini</option>
                                                <option>Prime</option>
                                                <option>Prime SUV</option>
                                                <option>Luxury Cars</option>
                                                <option>Mini Van</option>
                                                <option>Small Bus</option>
                                                <option>Luxury Bus</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s6">
                                        <div class="select-wrapper"><span class="caret">▼</span>
                                            <!-- <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-26a8bb1c-4c55-a565-6b6d-56881552db27" value="Total passengers"> -->
                                            <select name="total_passenger" class="initialized">
                                                <option value="" disabled="" selected="">Total passengers</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>10</option>
                                                <option>15</option>
                                                <option>20</option>
                                                <option>50</option>
                                                <option>100</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <div class="select-wrapper"><span class="caret">▼</span>
                                            <!-- <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-9906e118-74f4-7e7a-7364-a37f719036df" value="No of adults"> -->
                                            <select name="no_of_adults" class="initialized">
                                                <option value="" disabled="" selected="">No of adults</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s6">
                                        <div class="select-wrapper"><span class="caret">▼</span>
                                            <!-- <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-d172dcf1-9634-6978-2b49-fc0dd63bc743" value="No of childrens"> -->
                                            <select name="no_of_child" class="initialized">
                                                <option value="" disabled="" selected="">No of childrens</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s6">
                                        <div class="select-wrapper"><span class="caret">▼</span>
                                            <!-- <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-d3168267-1b88-85f9-d8a6-92c37f6143a3" value="Min Price"> -->
                                            <select name="min_price" class="initialized">
                                                <option value="" disabled="" selected="">Min Price</option>
                                                <option>$200</option>
                                                <option>$500</option>
                                                <option>$1000</option>
                                                <option>$5000</option>
                                                <option>$10,000</option>
                                                <option>$50,000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-field col s6">
                                        <div class="select-wrapper"><span class="caret">▼</span>
                                            <!-- <input type="text" class="select-dropdown" readonly="true" data-activates="select-options-97a029f9-2e07-94c9-f06a-027ff4e83d60" value="Max Price"> -->
                                            <select name="max_price" class="initialized">
                                                <option value="" disabled="" selected="">Max Price</option>
                                                <option>$200</option>
                                                <option>$500</option>
                                                <option>$1000</option>
                                                <option>$5000</option>
                                                <option>$10,000</option>
                                                <option>$50,000</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input type="text" id="first_name" name="first_name" style="background-color: #cecece !important">
                                        <label for="from">First Name</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input type="text" id="mobile_no" name="mobile_no" style="background-color: #cecece">
                                        <label for="to">Mobile No</label>
                                    </div>
                                </div>
                                <!--                            <div class="row">-->
                                <!--                                <div class="input-field col s6">-->
                                <!--                                    <input type="text" id="email" name="email"-->
                                <!--                                           style="background-color: #cecece !important">-->
                                <!--                                    <label for="from">Email</label>-->
                                <!--                                </div>-->
                                <!--                                <div class="input-field col s6">-->
                                <!--                                    <input type="text" id="mobile_no" name="mobile_no"-->
                                <!--                                           style="background-color: #cecece">-->
                                <!--                                    <label for="to">Mobile No</label>-->
                                <!--                                </div>-->
                                <!--                            </div>-->
                                <div class="row">
                                    <div class="input-field col s12">
                                        <i class="waves-effect waves-light tourz-sear-btn v2-ser-btn waves-input-wrapper" style=""><input type="submit" value="Request" class="waves-button-input"></i>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="v2-ho-se-ri">
                            <h1>Car Rentals easy now!</h1>
                            <p>Experience the various exciting tour and travel packages and Make hotel reservations, find vacation packages, search cheap hotels and events</p>
                            <div class="tourz-hom-ser v2-hom-ser">
                                <ul>
                                    <li>
                                        <a href="https://web.archive.org/web/20181011123028/http://worldgatecard.com/flightList" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="./car_rent_files/31.png" alt=""> Flight</a>
                                    </li>
                                    <li>
                                        <a href="https://web.archive.org/web/20181011123028/http://worldgatecard.com/carList" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="./car_rent_files/30.png" alt=""> Car Rentals</a>
                                    </li>
                                    <li>
                                        <a href="https://web.archive.org/web/20181011123028/http://worldgatecard.com/hotelList" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="./car_rent_files/1.png" alt=""> Hotel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END HEADER SECTION-->
    <style>
        .select-dropdown {
            background-color: #cecece !important;
        }
    </style>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <section>
        <div class="rows tips tips-home tb-space home_title">
            <div class="container tips_1">
                <!-- TIPS BEFORE TRAVEL -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h3>Tips Before Travel</h3>
                    <div class="tips_left tips_left_1">
                        <h5>Bring copies of your passport</h5>
                        <p>It is good idea to keep a copy of your passport will traveling outside your own country.</p>
                    </div>
                    <div class="tips_left tips_left_2">
                        <h5>Register with your embassy</h5>
                        <p>Let your embassy know your are traveling in country so in any emergency will find your full.
                        </p>
                    </div>
                    <div class="tips_left tips_left_3">
                        <h5>Always have local cash</h5>
                        <p>Get your currency exchange before traveling at correct rate help will buying things. </p>
                    </div>
                </div>
                <!-- CUSTOMER TESTIMONIALS -->
                <div class="col-md-8 col-sm-6 col-xs-12 testi-2">
                    <!-- TESTIMONIAL TITLE -->
                    <h3>Customer Testimonials</h3>
                    <div class="testi">
                        <h4>Gaurish Naresh Rane</h4>
                        <p>Amazing experience with World Gate got a really good deal on hotel and flight travel. </p>
                        <address>Manama, Bahrain</address>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>