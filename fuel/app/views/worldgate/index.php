<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->

    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!-- Header & Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Header & Navigation -->

    <!--HEADER SECTION-->

    <section>
        <div class="tourz-search">
            <div class="container">
                <div class="row">
                    <div class="tourz-search-1">
                        <h1>Gate Way for Great Offers!</h1>
                        <p>Experience the various exciting offer on Hotels, Flight &amp; Car Rentals. With World Gate
                            Membership
                            Plan.</p>
                        <!--                    <form class="tourz-search-form">-->
                        <!--                        <div class="input-field">-->
                        <!--                            <input type="text" id="select-city" class="autocomplete">-->
                        <!--                            <label for="select-city">Enter city</label>-->
                        <!--                        </div>-->
                        <!--                        <div class="input-field">-->
                        <!--                            <input type="text" id="select-search" class="autocomplete">-->
                        <!--                            <label for="select-search" class="search-hotel-type">Search over a million hotels, tour and-->
                        <!--                                travels, sight seeings and more</label>-->
                        <!--                        </div>-->
                        <!--                        <div class="input-field">-->
                        <!--                            <input type="submit" value="search" class="waves-effect waves-light tourz-sear-btn"></div>-->
                        <!--                    </form>-->
                        <div class="tourz-hom-ser">
                            <ul>
                                <li>
                                    <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>hotel_list" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/1.png" alt="">Hotel</a>
                                </li>
                                <li>
                                    <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>flight_list" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/31.png" alt="">Flight</a>
                                </li>
                                <li>
                                    <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/'; ?>car_rent" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/30.png" alt="">Car
                                        Rentals</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END HEADER SECTION-->
    <!--====== Benfits with World gate member ==========-->
    <section>
        <div class="foot-mob-sec tb-space">
            <div class="rows container">
                <!-- FAMILY IMAGE(YOU CAN USE ANY PNG IMAGE) -->
                <div class="col-md-6 col-sm-6 col-xs-12 family"><img src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/mobile.png" alt=""></div>
                <!-- REQUEST A QUOTE -->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <!-- THANK YOU MESSAGE -->
                    <div class="foot-mob-app">
                        <h2>Join The World Gate <br>VIP Program</h2>
                        <p>The World Gate invites you to join and enjoy fabulous room rates and many more facilities
                            with
                            us. With amazing complimentary vouchers, being a world gate member now!</p>
                        <ul>
                            <li><i class="fa fa-check" aria-hidden="true"></i> 10% Upto 20% Off Best Available Room
                                Rate.</li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Best Price when booking your airline
                                tickets.
                            </li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Best price for car rentals around the
                                world.
                            </li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> Assistance in issuing the visa to any
                                country
                                in the world.
                            </li>
                        </ul>

                        <div class="row">
                            <div class="col-md-4">
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                    <input type="hidden" name="cmd" value="_s-xclick">
                                    <input type="hidden" name="hosted_button_id" value="W3KLX6QHHHH8N">
                                    <input type="image" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/paypal-buttons.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" class="mouse-pointer" style="width: 121%">
                                    <img alt="" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/pixel.gif" class="mouse-pointer">
                                </form>
                            </div>
                            <div class="col-md-4">
                                <a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/paytab'; ?>">
                                    <img alt="" border="0" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/img/paytab-logo.png'; ?>" style="width: 90%; margin-left: 25px;" class="mouse-pointer">
                                </a>
                            </div>
                            
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .mouse-pointer {
            cursor: pointer;
        }
    </style>


    <!--====== Booking box  ==========-->
    <section>
        <div class="rows tb-space pad-top-o pad-bot-redu">
            <div class="container">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title">
                    <br> <br>
                    <h2>Hotels booking open now! </h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>Find your ideal hotel for the best price &amp; offers. Over 40,000 hotels to choose from across
                        50
                        countries.</p>
                </div>
                <!-- HOTEL GRID -->
                <div class="to-ho-hotel">
                    <!-- HOTEL GRID -->
                    <div class="col-md-12">
                        <ins class="bookingaff" data-aid="1448442" data-target_aid="1448442" data-prod="sbp" data-width="1100" data-height="300" data-lang="xu" data-currency="BHD" data-cc1="eg" data-df_num_properties="3">
                            <!-- Anything inside will go away once widget is loaded. -->
                            <a href="//www.booking.com?aid=1448442">Booking.com</a>
                        </ins>
                        <script type="text/javascript">
                            (function(d, sc, u) {
                                var s = d.createElement(sc),
                                    p = d.getElementsByTagName(sc)[0];
                                s.type = 'text/javascript';
                                s.async = true;
                                s.src = u + '?v=' + (+new Date());
                                p.parentNode.insertBefore(s, p);
                            })(document, 'script', '//cf.bstatic.com/static/affiliate_base/js/flexiproduct.js');
                        </script>
                        <br> <br>
                        <ins class="bookingaff" data-aid="1448442" data-target_aid="1448442" data-prod="sbp" data-width="1100" data-height="300" data-lang="xu" data-currency="BHD" data-cc1="tr" data-df_num_properties="3">
                            <!-- Anything inside will go away once widget is loaded. -->
                            <a href="//www.booking.com?aid=1448442">Booking.com</a>
                        </ins>
                        <script type="text/javascript">
                            (function(d, sc, u) {
                                var s = d.createElement(sc),
                                    p = d.getElementsByTagName(sc)[0];
                                s.type = 'text/javascript';
                                s.async = true;
                                s.src = u + '?v=' + (+new Date());
                                p.parentNode.insertBefore(s, p);
                            })(document, 'script', '//cf.bstatic.com/static/affiliate_base/js/flexiproduct.js');
                        </script>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--====== Booking box  ==========-->

    <!--====== Popular hotels ==========-->
    <section>
        <div class="rows tb-space pad-top-o pad-bot-redu">
            <div class="container">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title">
                    <h2>Popular <span>Hotels</span> </h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <!-- <p>World's leading Hotel Booking website,Over 3,000 Hotel rooms worldwide. Book Hotel rooms and enjoy your holidays with distinctive experience</p> -->
                </div>
                <!-- CITY -->
                <?php $bt_url = Uri::base(false); ?>
                <div class="col-md-3">
                    <a href="#">
                        <div class="tour-mig-like-com">
                            <div class="tour-mig-lc-img"> <img src="<?php echo $bt_url . 'assets/images/listing/logo_1.jpg'; ?>" alt=""> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#">
                        <div class="tour-mig-like-com">
                            <div class="tour-mig-lc-img"> <img src="<?php echo $bt_url . 'assets/images/listing/logo_2.jpg'; ?>" alt=""> </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#">
                        <div class="tour-mig-like-com">
                            <div class="tour-mig-lc-img"> <img src="<?php echo $bt_url . 'assets/images/listing/logo_3.jpg'; ?>" alt=""> </div>

                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#">
                        <div class="tour-mig-like-com">
                            <div class="tour-mig-lc-img"> <img src="<?php echo $bt_url . 'assets/images/listing/logo_4.jpg'; ?>" alt=""> </div>

                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#">
                        <div class="tour-mig-like-com">
                            <div class="tour-mig-lc-img"> <img src="<?php echo $bt_url . 'assets/images/listing/logo_5.jpg'; ?>" alt=""> </div>

                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#">
                        <div class="tour-mig-like-com">
                            <div class="tour-mig-lc-img"> <img src="<?php echo $bt_url . 'assets/images/listing/logo_6.jpg'; ?>" alt=""> </div>

                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#">
                        <div class="tour-mig-like-com">
                            <div class="tour-mig-lc-img"> <img src="<?php echo $bt_url . 'assets/images/listing/logo_7.jpg'; ?>" alt=""> </div>

                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#">
                        <div class="tour-mig-like-com">
                            <div class="tour-mig-lc-img"> <img src="<?php echo $bt_url . 'assets/images/listing/logo_8.jpg'; ?>" alt=""> </div>

                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!--====== Popular hotels ==========-->
    <!-- Offer Block-->
    <!-- <section>
        <div class="rows pad-bot-redu tb-space">
            <div class="container">
                <div class="spe-title">
                    <h2>Our <span>Top Offers</span></h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>Over 10,000 Offers & Packages worldwide just a click way <a href="#">Be Member</a></p>
                </div>
                <div>

                    <div class="col-md-4 col-sm-6 col-xs-12 b_packages">

                        <div class="band"><img src="<?php //echo Uri::base(false) . 'assets/' 
                                                    ?>images/band.png" alt="" />
                        </div>
                        IMAGE
                        <div class="v_place_img"><img src="<?php //echo Uri::base(false) . 'assets/' 
                                                            ?>images/t5.png" alt="Tour Booking" title="Tour Booking" /></div>
                        TOUR TITLE & ICONS
                        <div class="b_pack rows">
                            TOUR TITLE
                            <div class="col-md-8 col-sm-8">
                                <h4><a href="tour-details.html">Rio de Janeiro<span class="v_pl_name">(Brazil)</span></a>
                                </h4>
                            </div>
                            TOUR ICONS
                            <div class="col-md-4 col-sm-4 pack_icon">
                                <ul>
                                    <li>
                                        <a href="#"><img src="<?php //echo Uri::base(false) . 'assets/' 
                                                                ?>images/clock.png" alt="Date" title="Tour Timing" /> </a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="<?php //echo Uri::base(false) . 'assets/' 
                                                                ?>images/info.png" alt="Details" title="View more details" /> </a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="<?php //echo Uri::base(false) . 'assets/' 
                                                                ?>images/price.png" alt="Price" title="Price" /> </a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="<?php //echo Uri::base(false) . 'assets/' 
                                                                ?>images/map.png" alt="Location" title="Location" /> </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!--====== POPULAR TOUR PLACES ==========-->

    <script>
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        var s = getUrlParameter('location');
        console.log('get url ', s);
    </script>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/customer-testimonial'); ?>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>



    <div class="hiddendiv common"></div>
</body>

</html>