<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!--Header & Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Header & Navigation -->

    <!--End HEADER SECTION-->

    <!--====== LOCATON ==========-->
    <section>
        <div class="rows inner_banner inner_banner_1" style="padding-top: 210px;">
            <div class="container">
                <h2>Contact us</h2>
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                    </li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="#" class="bread-acti">Contact Us</a></li>
                </ul>
                <p>Fill free to contact us.</p>
            </div>
        </div>
    </section>
    <!--====== QUICK ENQUIRY FORM ==========-->
    <section>
        <div class="form form-spac rows">
            <div class="container">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2>Holiday Tour <span>Contact us</span></h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <!--                <p>World's leading tour and travels Booking website,Over 30,000 packages worldwide. Book travel packages-->
                    <!--                    and enjoy your holidays with distinctive experience</p>-->
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form_1 wow fadeInLeft animated" data-wow-duration="1s" style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;">
                    <!--====== THANK YOU MESSAGE ==========-->
                    <div class="succ_mess">Thank you for contacting us we will get back to you soon.</div>
                    <form id="home_form" name="home_form" action="https://web.archive.org/web/20181011121224/http://rn53themes.net/themes/demo/travelz/mail/send.php">
                        <ul>
                            <li>
                                <input type="text" name="ename" value="" id="ename" placeholder="Name" required="">
                            </li>
                            <li>
                                <input type="tel" name="emobile" value="" id="emobile" placeholder="Mobile" required="">
                            </li>
                            <li>
                                <input type="email" name="eemail" value="" id="eemail" placeholder="Email id" required="">
                            </li>
                            <li>
                                <input type="text" name="esubject" value="" id="esubject" placeholder="Subject" required="">
                            </li>
                            <li>
                                <input type="text" name="ecity" value="" id="ecity" placeholder="City" required="">
                            </li>
                            <li>
                                <input type="text" name="ecount" value="" id="ecount" placeholder="Country" required="">
                            </li>
                            <li>
                                <textarea name="emess" cols="40" rows="3" id="text-comment" placeholder="Enter your message"></textarea>
                            </li>
                            <li>
                                <input type="submit" value="Submit" id="send_button">
                            </li>
                        </ul>
                    </form>
                </div>
                <!--====== COMMON NOTICE ==========-->
                <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight animated" data-wow-duration="1s" style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;">
                    <div class="rows book_poly">
                        <!--                    <h4><b>Contact No:</b></h4>-->
                        <h3 style="padding-bottom: 2px !important;">Contact No</h3>
                        <h4>
                            +973 3414 0420<br>
                        </h4>
                        <h3 style="padding-bottom: 2px !important;">Address</h3>
                        <h4>
                            Office 1640, Bldg 1565,<br>
                            Road No. 1722, Block 317,<br>
                            Diplomat Commercial Office Tower,<br>
                            Diplomatic Area, Manama,<br>
                            Kingdom of Bahrain<br>
                        </h4>

                        <!--                    <ul>-->
                        <!--                        <li> Office 1640, Bld 1565,</li>-->
                        <!--                        <li>Road No. 1722, Block 317,</li>-->
                        <!--                        <li>Diplomatic Area, Manama,</li>-->
                        <!--                        <li>Kingdom of Bahrain</li>-->
                        <!--                    </ul>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="form form-spac rows">
            <div class="container">
                <div class="col-md-4 col-sm-4 col-xs-12 form_1 wow fadeInLeft animated">
                    <h3>Bahrain</h3>
                    <h4>
                        Office 1640, Bldg. 1565, Road 1722, <br>
                        Block 317, Diplomatic Area, Manama.<br>
                        Mobile Support: +97334140420
                    </h4>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 form_1 wow fadeInLeft animated">
                    <h3>Egypt</h3>
                    <h4>
                        25 B Khufu Entrance, <br>
                        Hadaiek Al Ahram, Jizah<br>
                        Land Line: +20233769902<br>
                        Mobile Support: +201555254426
                    </h4>

                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 form_1 wow fadeInLeft animated">
                    <h3> Saudi Arabia</h3>
                    <h4>
                        Jeddah & Riyadh. <br>
                        Land Line: +966543664212
                    </h4>
                </div>
            </div>
        </div>
    </section>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <section>
        <div class="rows tips tips-home tb-space home_title">
            <div class="container tips_1">
                <!-- TIPS BEFORE TRAVEL -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h3>Tips Before Travel</h3>
                    <div class="tips_left tips_left_1">
                        <h5>Bring copies of your passport</h5>
                        <p>It is good idea to keep a copy of your passport will traveling outside your own country.</p>
                    </div>
                    <div class="tips_left tips_left_2">
                        <h5>Register with your embassy</h5>
                        <p>Let your embassy know your are traveling in country so in any emergency will find your full.
                        </p>
                    </div>
                    <div class="tips_left tips_left_3">
                        <h5>Always have local cash</h5>
                        <p>Get your currency exchange before traveling at correct rate help will buying things. </p>
                    </div>
                </div>
                <!-- CUSTOMER TESTIMONIALS -->
                <div class="col-md-8 col-sm-6 col-xs-12 testi-2">
                    <!-- TESTIMONIAL TITLE -->
                    <h3>Customer Testimonials</h3>
                    <div class="testi">
                        <h4>Gaurish Naresh Rane</h4>
                        <p>Amazing experience with World Gate got a really good deal on hotel and flight travel. </p>
                        <address>Manama, Bahrain</address>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>