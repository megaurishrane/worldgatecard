<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!-- Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Navigation -->

    <!-- END HEADER SECTION -->

    <!--====== Benfits with World gate member ==========-->
    <section>
        <div class="rows inner_banner inner_banner_1" style="padding-top: 210px;">
            <div class="container">
                <h2>Classic Member Benefits</h2>
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                    </li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits_saudi'; ?>">Saudi Member Benefits</a></li>
                    <li><a href="#" class="bread-acti">Classic Membership</a></li>
                </ul>
                <p>Join and enjoy fabulous room rates and many more facilities with us.</p>
            </div>
        </div>
    </section>
    <!--====== ALL POST ==========-->
    <section>
        <div class="rows inn-page-bg com-colo">
            <div class="container inn-page-con-bg tb-space pad-bot-redu-5" id="inner-page-title">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2>World Gate <span>CLASSIC</span> Program</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>The World Gate invites you to join and enjoy fabulous room rates and many more facilities with
                        us.
                        With amazing complimentary vouchers, being a world gate member now!</p>
                </div>
                <!--===== POSTS ======-->
                <div class="rows">
                    <div class="posts">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h3 style="color:#AD974F;">Our Dear Partner.</h3>
                            <p style="font-size: larger;">You’re joining the "Classic" program is a great addition to us, and all we seek is to provide the best service and the lowest prices. We are described as an authorized agent of Booking.com to ensure your trust and permanent loyalty to us in all areas of tourism and travel.
                                <br><br>
                                Your participation with us in the "Classic" program is a lifetime subscription for you and your family, and your personal vouchers will be renewed after each year from the date of subscription according to your desire.
                            </p>
                            <h3 style="color:#AD974F;">Basic Lifetime Features of the "Classic" Program.</h3>

                            <h4 style="color: #8E793E;">• Up to 20% discount on hotel & hotel apartment rates lower than the prices of the global booking site Booking.com</h4>
                            <h4 style="color: #8E793E;">• Flight reservation service on all airlines around the world at the best prices with the provision of service around the clock.</h4>
                            <h4 style="color: #8E793E;">• Up to 15% discount on car rental service in all countries of the world.</h4>
                            <h4 style="color: #8E793E;">• Visa service for any country in the world at special prices.</h4>

                            <br><br>
                            <h3 style="color:#AD974F;">Annually renewed benefits from the "Classic" program</h3>

                            <h3 style="color:#AD974F;">Free Nights:</h3>
                            <p style="font-size: larger;">• 1 free night stay only in one of the 4 and 5 star hotels in any country in the world in any hotel of your choice or available alternatives.</br></br>
                                • 1 free night, staying only in one of the 4 and 5 star hotels, booking 4 nights for only 3 nights in Cairo in any hotel of your choice or available alternatives.</br></br>
                                • 1 free night, staying only in one of the 4 and 5 star hotels, booking 4 nights for only 3 nights in Bahrain in any hotel of your choice or available alternatives.</br></br>
                                • 1 free night, staying only in one of the 4 and 5 star hotels, booking 4 nights for only 3 nights in Istanbul in any hotel of your choice or available alternatives.</br></br>
                                • 1 free night, staying only in one of the 4 and 5 star hotels, Booking 4 nights for only 3 nights in Dubai in any hotel of your choice or available alternatives.</br></br>
                                • 1 free night, staying only in one of the 4 and 5 star hotels, booking 4 nights for only 3 nights in Beirut in any hotel of your choice or available alternatives.</br></br>
                                • 1 free night, staying only in one of the 4 and 5 star hotels, booking for 4 nights for only 3 nights in Amman, Jordan, in any hotel of your choice or available alternatives.</p>

                            <h5 class="desc-p"></h5>

                            <h3 style="color:#AD974F;">Flight and delivery:</h3>
                            <p style="font-size: larger;">• 1 discount coupon of 500 Saudi riyals on airline tickets when booking 7 consecutive nights in any five-star hotel of your choice or any of the alternatives available in the Middle East, East Asia or Europe.</p>
                            <p style="font-size: larger;">• 1 free flight ticket coupon (round trip) when booking 7 consecutive nights in any five-star hotel in ISTANBUL or Middle East except (Tunisia - Algeria - Morocco) of your choice or in any of the alternatives available on the same date and place of booking.</p>
                            <p style="font-size: larger;">• 50% discount on airport transfers in Europe and East Asia when booking 5 consecutive nights.</p>
                            <p style="font-size: larger;">• Free transportation from or to the airport in (Cairo / Dubai / Turkey) when booking 4 consecutive nights.</p>
                            <p style="font-size: larger;">• Discount on meet and assist at (Cairo / Dubai) airports.</p>

                            <h3 style="color:#AD974F;">Additional entertainment features:</h3>
                            <p style="font-size: larger;">• 1 Free dinner coupon for two people on one of the Nile boats in Cairo.</p>
                            <p style="font-size: larger;">• 1 coupon 250 USD discount when booking 5 nights on Maldives resorts.</p>


                            <h3 style="color:#AD974F;">Other tourist services:</h3>
                            <p style="font-size: larger;">• Reservation and organization of medical trips in the most famous specialized medical centers in the world, accompanied by an interpreter.</p>
                            <p style="font-size: larger;">• Book tickets for international matches at special prices.</p>
                            <p style="font-size: larger;">• Organizing various tourism programs for the honeymoon.</p>
                            <p style="font-size: larger;">• Register on our website and get an activation code once you subscribe.</p>

                            <h3 style="color:#AD974F;">Terms and Conditions:</h3>
                            <p style="font-size: larger;">• All prices are compared to the rates published on the global booking site Booking.com.</p>
                            <p style="font-size: larger;">• We guarantee to provide the lowest price on all reservations throughout the year.</p>
                            <p style="font-size: larger;">• Reservations are based on the availability at the time of the reservation request.</p>
                            <p style="font-size: larger;">• A fair use policy applies.</p>




                            <a href="https://web.archive.org/web/20181114145021/http://worldgatecard.com/register" class="link-btn">Register Now</a>
                        </div>
                    </div>
                </div>
                <!--===== POST END ======-->
            </div>
        </div>
    </section>

    <style>
        .desc-p {

            font-style: normal !important;
            font-weight: 500 !important;
            margin-bottom: 20px !important;
        }
    </style>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <section>
        <div class="rows tips tips-home tb-space home_title">
            <div class="container tips_1">
                <!-- TIPS BEFORE TRAVEL -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h3>Tips Before Travel</h3>
                    <div class="tips_left tips_left_1">
                        <h5>Bring copies of your passport</h5>
                        <p>It is good idea to keep a copy of your passport will traveling outside your own country.</p>
                    </div>
                    <div class="tips_left tips_left_2">
                        <h5>Register with your embassy</h5>
                        <p>Let your embassy know your are traveling in country so in any emergency will find your full.
                        </p>
                    </div>
                    <div class="tips_left tips_left_3">
                        <h5>Always have local cash</h5>
                        <p>Get your currency exchange before traveling at correct rate help will buying things. </p>
                    </div>
                </div>
                <!-- CUSTOMER TESTIMONIALS -->
                <div class="col-md-8 col-sm-6 col-xs-12 testi-2">
                    <!-- TESTIMONIAL TITLE -->
                    <h3>Customer Testimonials</h3>
                    <div class="testi">
                        <h4>Gaurish Naresh Rane</h4>
                        <p>Amazing experience with World Gate got a really good deal on hotel and flight travel. </p>
                        <address>Manama, Bahrain</address>
                    </div>
                    <!-- ARRANGEMENTS & HELPS -->
                    <!--                <h3>Arrangement & Helps</h3>-->
                    <!--                <div class="arrange">-->
                    <!--                    <ul>-->
                    <!--                    -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="-->
                    <!--images/Location-Manager.png"-->
                    <!--                                             alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                    -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="-->
                    <!--images/Private-Guide.png"-->
                    <!--                                             alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                      -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="-->
                    <!--images/Arrangements.png"-->
                    <!--                                             alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                     -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img-->
                    <!--                                        src="-->
                    <!--images/Events-Activities.png"-->
                    <!--                                        alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                    </ul>-->
                    <!--                </div>-->
                </div>
            </div>
        </div>
    </section>

    <section>

    </section>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>