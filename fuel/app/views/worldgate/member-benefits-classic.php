<!DOCTYPE html>
<!-- saved from url=(0068)https://web.archive.org/web/20181106161332/http://worldgatecard.com/ -->
<html lang="en">

<head>
    <?php echo Fuel\Core\View::forge('worldgate/components/headerFiles'); ?>
</head>

<body data-new-gr-c-s-check-loaded="14.1029.0" data-gr-ext-installed="" style="overflow: visible;">
    <!-- BEGIN WAYBACK TOOLBAR INSERT -->
    <style type="text/css">
        body {
            margin-top: 0 !important;
            padding-top: 0 !important;
            /*min-width:800px !important;*/
        }
    </style>

    <div id="donato" style="position:relative;width:100%;">
        <div id="donato-base">
            <iframe id="donato-if" src="<?php echo Fuel\Core\Uri::base(false) . 'assets/all'; ?>/donate.html" scrolling="no" frameborder="0" style="width:100%; height:100%">
            </iframe>
        </div>
    </div>

    <!-- END WAYBACK TOOLBAR INSERT -->
    <!-- 
    <audio src="./assets/worldgate_background_cutteversion.mp4" controls="" autoplay="" loop="" class="audio-music">
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> -->
    <style>
        .audio-music {
            display: none;
        }
    </style>
    <!-- <embed src="./assets/worldgate_background_cutteversion.mp4" width="180" height="90" loop="false" autostart="false" hidden="true" /> -->
    <!-- Preloader -->
    <div id="preloader" style="display: none;">
        <div id="status" style="display: none;">&nbsp;</div>
    </div>

    <!-- Navigation -->
    <?php echo \Fuel\Core\View::forge('worldgate/components/navigation'); ?>
    <!-- End Navigation -->

    <!-- END HEADER SECTION -->

    <!--====== Benfits with World gate member ==========-->
    <section>
        <div class="rows inner_banner inner_banner_1" style="padding-top: 210px;">
            <div class="container">
                <h2>Classic Member Benefits</h2>
                <ul>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false); ?>">Home</a>
                    </li>
                    <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                    <li><a href="<?php echo \Fuel\Core\Uri::base(false) . 'welcome/member_benefits'; ?>">Member Benefits</a></li>
                    <li><a href="#" class="bread-acti">Classic Membership</a></li>
                </ul>
                <p>Join and enjoy fabulous room rates and many more facilities with us.</p>
            </div>
        </div>
    </section>
    <!--====== ALL POST ==========-->
    <section>
        <div class="rows inn-page-bg com-colo">
            <div class="container inn-page-con-bg tb-space pad-bot-redu-5" id="inner-page-title">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2>World Gate <span>CLASSIC</span> Program</h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>The World Gate invites you to join and enjoy fabulous room rates and many more facilities with
                        us.
                        With amazing complimentary vouchers, being a world gate member now!</p>
                </div>
                <!--===== POSTS ======-->
                <div class="rows">
                    <div class="posts">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h4 style="color: #8E793E;">• 10% UP TO 20% discount for you on the price through Booking.com Four & Five Stars Hotels.</h4>
                            <h4 style="color: #8E793E;">• Best Price when booking your airline tickets & renting cars.</h4>
                            <h4 style="color: #8E793E;">• Helping to issue visa to various destinations around the world.</h4>
                            </h4>
                            <br><br>
                            <h3 style="color:#AD974F;">CLASSIC MEMBERSHIP Certificates</h3>
                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">1 x Complementary night in GCC (Non-Transferable)</strong>
                                <br>
                                one (1) certificate complementary night at any participating 4* hotels for two (2) persons in a standard room at any GCC countries
                            </h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">1 x (2 for 1) Certificate in BAHRAIN (Transferable)</strong><br>
                                one (1) certificate to enjoy the second night free when paying for the first night at any participating hotels in Bahrain.</h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">1 x (3 for 2) Certificate in DUBAI (Transferable):</strong><br>
                                one (1) certificate to enjoy the third night free when paying for the first two nights at any participating hotels in Dubai.</h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">1 x (4 for 3) Certificate in EUROPE (Transferable): </strong><br>one (1) certificate to enjoy the fourth night free when paying for the
                                first three nights at any participating hotels in Western Europe.</h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">1 x (4 for 3) Certificate BANGKOK (Transferable): </strong><br>
                                one (1) certificate to enjoy the fourth night free when paying for the first three nights at any participating hotels in Bangkok.
                            </h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">1 x (4 for 3) Certificate BAKU (Transferable):</strong><br>
                                one (1) certificate to enjoy the fourth night free when paying for the first three nights at any participating hotels in Baku. </h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">1 x (4 for 3) Certificate MAKKAH (NON-Transferable): </strong><br>
                                one (1) certificate to enjoy the fourth night free when paying for the first three nights at any participating hotels in Makkah. </h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">1 x (4 for 3) Certificate in ISTANBUL (Transferable): </strong>
                                <br>one (1) certificate to enjoy the fourth night free when paying for the first three nights at any participating hotels in Istanbul.
                            </h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">1 x Free Airline Ticket in DUBAI (NON-Transferable): </strong> <br>
                                one (1) certificate free airline ticket in case of booking accommodation for four (4) consecutive nights at any participating five stars hotels. </h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">2 x Free Transfer (Transferable): </strong>
                                <br> Two (2) certificates one-way airport transfer WORLDWIDE in case of booking five stars hotels through us.
                            </h5>

                            <h5 class="desc-p">•&nbsp;<strong style="color: #8E793E;">5 x 10 BHD Free Vouchers (Transferable): </strong>
                                <br> Five (5) certificates will deduct 10 BHD WORLDWIDE in case of booking 4 & 5 stars hotels through us.
                            </h5>

                            <h3 style="color:#AD974F;">Other Benefits</h3>
                            <h5 class="desc-p">• As a WORLD GATE member, you are entitled to receive 20% off B.A.R. (best available published rate) on all room categories.
                            </h5>

                            <h5 class="desc-p">• A valid Membership Card number must be quoted at the time of reservation.
                                Subject to room allotment &amp; availability. Benefit is valid for direct bookings only.
                            </h5>

                            <h5 class="desc-p">• Room rate should be off best available rate of the day or any available internet
                                rates. No discount on corporate rates or promotional rates.
                            </h5>
                            <h5 class="desc-p">
                                • To take advantage of these special rates, simply provide your membership number when
                                calling one of our worldwide reservations offices or when calling the hotel directly.
                            </h5>
                            <h4>
                                Please check the website for the participating hotels local benefits when you travel since each participating hotel local benefits may vary.
                            </h4>
                            <h3 style="margin-top: 50px;color:#AD974F;">Complimentary Upgrade</h3>
                            <h5 class="desc-p">
                                • Members shall be entitled to receive an Upgrade to the next available room category at the preferred room rate,
                            </h5>
                            <h5 class="desc-p">
                                • Subject to availability. Benefit is valid for direct bookings only.
                            </h5>
                            <h5 class="desc-p">
                                • Membership number must be provided at time of advance reservation for Member and/or guest
                            </h5>
                            <h3 style="margin-top: 50px;color:#AD974F;">Flexible Check-In / Check-Out</h3>
                            <h5 class="desc-p">
                                • Flexible check in, not to exceed four (4) hours before posted time if available.
                            </h5>
                            <h5 class="desc-p">
                                • Flexible check out, not to exceed four (4) hours after posted time if available
                            </h5>
                            <h5 class="desc-p">
                                • Subject to availability
                            </h5>
                            <a href="https://web.archive.org/web/20181114145021/http://worldgatecard.com/register" class="link-btn">Register Now</a>
                        </div>
                    </div>
                </div>
                <!--===== POST END ======-->
            </div>
        </div>
    </section>

    <style>
        .desc-p {

            font-style: normal !important;
            font-weight: 500 !important;
            margin-bottom: 20px !important;
        }
    </style>

    <!--====== FOOTER 1 ==========-->
    <!--====== TIPS BEFORE TRAVEL ==========-->
    <section>
        <div class="rows tips tips-home tb-space home_title">
            <div class="container tips_1">
                <!-- TIPS BEFORE TRAVEL -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h3>Tips Before Travel</h3>
                    <div class="tips_left tips_left_1">
                        <h5>Bring copies of your passport</h5>
                        <p>It is good idea to keep a copy of your passport will traveling outside your own country.</p>
                    </div>
                    <div class="tips_left tips_left_2">
                        <h5>Register with your embassy</h5>
                        <p>Let your embassy know your are traveling in country so in any emergency will find your full.
                        </p>
                    </div>
                    <div class="tips_left tips_left_3">
                        <h5>Always have local cash</h5>
                        <p>Get your currency exchange before traveling at correct rate help will buying things. </p>
                    </div>
                </div>
                <!-- CUSTOMER TESTIMONIALS -->
                <div class="col-md-8 col-sm-6 col-xs-12 testi-2">
                    <!-- TESTIMONIAL TITLE -->
                    <h3>Customer Testimonials</h3>
                    <div class="testi">
                        <h4>Gaurish Naresh Rane</h4>
                        <p>Amazing experience with World Gate got a really good deal on hotel and flight travel. </p>
                        <address>Manama, Bahrain</address>
                    </div>
                    <!-- ARRANGEMENTS & HELPS -->
                    <!--                <h3>Arrangement & Helps</h3>-->
                    <!--                <div class="arrange">-->
                    <!--                    <ul>-->
                    <!--                    -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="-->
                    <!--images/Location-Manager.png"-->
                    <!--                                             alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                    -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="-->
                    <!--images/Private-Guide.png"-->
                    <!--                                             alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                      -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img src="-->
                    <!--images/Arrangements.png"-->
                    <!--                                             alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                     -->
                    <!--                        <li>-->
                    <!--                            <a href="#"><img-->
                    <!--                                        src="-->
                    <!--images/Events-Activities.png"-->
                    <!--                                        alt=""> </a>-->
                    <!--                        </li>-->
                    <!--                    </ul>-->
                    <!--                </div>-->
                </div>
            </div>
        </div>
    </section>

    <section>

    </section>

    <!--====== FOOTER 2 ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerNav'); ?>

    <!--====== FOOTER - COPYRIGHT ==========-->
    <?php echo \Fuel\Core\View::forge('worldgate/components/footerScript'); ?>

    <div class="hiddendiv common"></div>
</body>

</html>