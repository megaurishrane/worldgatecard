<!DOCTYPE html>
<html lang="en" ng-app="app">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin beta - numberbazaar.com</title>
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->
  <?php
    echo \Asset::css([
    'style.css',
    ]);
    ?>
</head>

<body ng-init='' class="navbar-fixed sidebar-nav fixed-nav">
  <!-- BODY options, add following classes to body to change options
    1. 'compact-nav'     	  - Switch sidebar to minified version (width 50px)
    2. 'sidebar-nav'		  - Navigation on the left
    2.1. 'sidebar-off-canvas'	- Off-Canvas
    2.1.1 'sidebar-off-canvas-push'	- Off-Canvas which move content
    2.1.2 'sidebar-off-canvas-with-shadow'	- Add shadow to body elements
    3. 'fixed-nav'			  - Fixed navigation
    4. 'navbar-fixed'		  - Fixed navbar
    5. 'footer-fixed'		  - Fixed navbar
    -->

  <!-- User Interface -->
  <ui-view></ui-view>

  <?php
    echo \Asset::js([
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/tether/dist/js/tether.min.js',
    'bower_components/bootstrap/dist/js/bootstrap.min.js',
    'bower_components/angular/angular.min.js',
    'bower_components/angular-ui-router/release/angular-ui-router.min.js',
    'bower_components/oclazyload/dist/ocLazyLoad.min.js',
    'bower_components/angular-breadcrumb/dist/angular-breadcrumb.min.js',
    'bower_components/angular-loading-bar/build/loading-bar.min.js'
    ]);
    ?>
    <!-- AngularJS CoreUI App scripts -->
    <?php
    echo \Asset::js([
    '../../app/app.js',
    '../../app/routes.js',
    '../../app/demo/routes.js',
    '../../app/controllers.js',
    '../../app/directives.js'
    ]);
    ?>


</body>

</html>