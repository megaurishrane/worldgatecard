<?php
/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 7/10/2017
 * Time: 8:48 AM
 */
namespace Fame;

use Fuel\Core\DB;

/**
 * Class Tree
 * List of tree types
 */
class CategoryMst {
    const table = 'category_mast';

    public static function get ($where = [], $select = null, $limit = false, $offset = 0) {

        $q = DB::select_array($select)->from(self::table)->order_by('parent_id')->where($where);

        if ($limit) {
            $q->limit($limit);
            if ($offset)
                $q->offset($offset);
        }

        $c = $q->compile();

        $query = Utils::sqlCalcRowInsert($c);

        $res = DB::query($query)->execute()->as_array();

        return count($res) ? $res : false;
    }

    /**
     * @param $set
     *
     * @return mixed
     * @internal param $tree_log_id
     * @internal param array $upload_ids
     */
    public static function insert ($set) {
        list($insert_id, $af) = DB::insert(self::table)->set($set)->execute();

        return $insert_id;
    }

    public static function update (Array $where, $set) {
        $af = DB::update(self::table)->set($set)->where($where)->execute();

        return $af;
    }

    public static function remove (Array $where) {
        $af = DB::delete(self::table)->where($where)->execute();

        return $af;
    }
}