<?php
/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 7/10/2017
 * Time: 8:48 AM
 */

namespace Fame;

use Fuel\Core\DB;

/**
 * Class Tree
 * List of tree types
 */
class AssetsMst
{
    const table = 'assets_mst';
    //todo: create as class for assets amount
    const asset_amount = 'assets_amount_mst';

    public static function get($where = [], $select = null, $limit = false, $offset = 0)
    {
        $q = DB::select_array($select)->from(self::table)->where($where);

        if ($limit) {
            $q->limit($limit);
            if ($offset)
                $q->offset($offset);
        }

        $c = $q->compile();

        $query = Utils::sqlCalcRowInsert($c);

        $res = DB::query($query)->execute();

        return count($res) ? $res : false;
    }


    public static function get_join($where = [], $select = null, $limit = false, $offset = 0)
    {
        $q = DB::query('SELECT SQL_CALC_FOUND_ROWS *, ' .
            'category_mast.name as main_category_name, ' .
            'category_mast.category_id as main_category_id, ' .
            'sub_category.name as sub_category_name, ' .
            'sub_category.category_id as sub_category_id ' .
            'FROM `assets_mst` ' .
            'JOIN ( ' .
            'SELECT * ' .
            'FROM assets_amount_mst ' .
            'WHERE assets_amount_id IN ( ' .
            'SELECT MAX(assets_amount_id) AS assets_amount_id ' .
            'FROM assets_amount_mst ' .
            'GROUP BY assets_id ' .
            'ORDER BY assets_amount_id DESC)) AS assets_amount ON(assets_amount.assets_id = assets_mst.assets_id) ' .
            'join `category_mast`' .
            'on (assets_mst.main_category_id = category_mast.category_id) ' .
            'join `category_mast` as sub_category ' .
            'on (assets_mst.sub_category_id = sub_category.category_id)
            where assets_mst.user_id = "' . $where['user_id'] . '"
            ')->execute()->as_array();

        return count($q) ? $q : [];
    }

    public static function get_worth($user_id, $category_type = 'main', $category_id = false)
    {
        if ($category_type == 'main') {
            $graph_name = 'category_mast.name as name,';
        } else {
            $graph_name = 'sub_category.name as name,';
        }
        $query = 'SELECT sum(amount) as `y`, ' .
            $graph_name .
            'assets_mst.main_category_id as graph_main_category_id, ' .
            'assets_mst.sub_category_id as graph_sub_category_id ' .
            'FROM `assets_mst` ' .
            'JOIN ( ' .
            'SELECT * ' .
            'FROM assets_amount_mst ' .
            'WHERE assets_amount_id IN ( ' .
            'SELECT MAX(assets_amount_id) AS assets_amount_id ' .
            'FROM assets_amount_mst ' .
            'GROUP BY assets_id ' .
            'ORDER BY assets_amount_id DESC)) AS assets_amount ON(assets_amount.assets_id = assets_mst.assets_id) ' .
            'join `category_mast`' .
            'on (assets_mst.main_category_id = category_mast.category_id) ' .
            'join `category_mast` as sub_category ' .
            'on (assets_mst.sub_category_id = sub_category.category_id) ' .
            'where assets_mst.user_id = "' . $user_id . '"';

        if ($category_type == 'main') {
            $query .= 'group by main_category_id';
        } else {
            $query .= 'where assets_mst.main_category_id = ' . $category_id . ' group by sub_category_id';
        }

        $database = DB::query($query)->execute()->as_array();

        foreach ($database as $k => $v) {
            $database[$k]['y'] = (int)$v['y'];
        }
        return count($database) ? $database : [];
    }

    /**
     * no more in use access from the asset control
     * @param $set
     * @deprecated
     */
    public
    static function insert($set)
    {
        list($insert_id, $af) = DB::insert(self::table)->set($set)->execute();

        return $insert_id;
    }

    /**
     * @param $set
     * @deprecated
     */
    public
    static function update(Array $where, $set)
    {
        $af = DB::update(self::table)->set($set)->where($where)->execute();

        return $af;
    }

    /**
     * @param $set
     * @deprecated
     */
    public
    static function remove(Array $where)
    {
        $af = DB::update(self::table)->set(['status' => 2])->where($where)->execute();
        return $af;
    }

    /**
     * @param $set
     * @deprecated
     */
    public
    static function remove_permanently(Array $where)
    {
        $af = DB::delete(self::table)->where($where)->execute();

        return $af;
    }
}
