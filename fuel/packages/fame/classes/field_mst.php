<?php
/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 1/25/2018
 * Time: 11:29 AM
 */

namespace Fame;

use Fuel\Core\DB;

/**
 * Class Tree
 * List of tree types
 */
class FieldMst
{
    const table = 'field_mst';

    public static function get($where = [], $select = null, $limit = false, $offset = 0)
    {

        $q = DB::select_array($select)->from(self::table)->where($where);

        if ($limit) {
            $q->limit($limit);
            if ($offset)
                $q->offset($offset);
        }

        $c = $q->compile();

        $query = Utils::sqlCalcRowInsert($c);

        $res = DB::query($query)->execute()->as_array();

        return count($res) ? $res : false;
    }

    public static function field_by_category($where = [], $select = null, $limit = false, $offset = 0)
    {
        //todo: create field allocation class later
        $field_allocation_tb = 'field_allocation_mst';

        $q = DB::select_array($select)->from(self::table)
            ->join($field_allocation_tb)
            ->on(self::table . '.field_id', '=', $field_allocation_tb . '.field_id')
            ->where($where);

        if ($limit) {
            $q->limit($limit);
            if ($offset)
                $q->offset($offset);
        }

        $c = $q->compile();

        $query = Utils::sqlCalcRowInsert($c);

        $res = DB::query($query)->execute()->as_array();

        if (count($res)) {
            foreach ($res as $k => $v) {
                $res[$k]['options'] = self::field_option(['field_id' => $v['field_id']]);
            }
            return $res;
        } else {
            return false;
        }

    }

    public static function field_option($where = [], $select = null, $limit = false, $offset = 0)
    {
        //todo: create the field option class laster
        $field_option_tb = 'field_option_mst';
        $q = DB::select_array($select)->from($field_option_tb)->where($where);

        if ($limit) {
            $q->limit($limit);
            if ($offset)
                $q->offset($offset);
        }

        $c = $q->compile();

        $query = Utils::sqlCalcRowInsert($c);

        $res = DB::query($query)->execute()->as_array();

        return count($res) ? $res : [];
    }
}

