<?php
/**
 * Created by PhpStorm.
 * User: Gaurish.r
 * Date: 7/2/2017
 * Time: 5:16 PM
 */
\Fuel\Core\Autoloader::add_classes([
    'Fame\Auth\Auth'                      => __DIR__ . '/classes/authenticate/auth.php',
    'Fame\Auth\Users'                     => __DIR__ . '/classes/authenticate/users.php',
    'Fame\Exception\AppException'         => __DIR__ . '/classes/exception/appexception.php',
    'Fame\Exception\UserException'        => __DIR__ . '/classes/exception/userexception.php',
    'Fame\Exception\ExceptionInterceptor' => __DIR__ . '/classes/exception/exceptioninterceptor.php',
    'Fame\Utils'                          => __DIR__ . '/classes/utils.php',

    'Fame\AssetsMst'                      => __DIR__.'./classes/assets_mst.php',
    'Fame\CategoryMst'                    => __DIR__.'./classes/category_mst.php',
    'Fame\FieldMst'                       => __DIR__.'./classes/field_mst.php'
]);
